<?php /* Template Name: Main page */ get_header();
global $options;
$pid=$post->ID;
$pmeta=get_post_meta( $pid, '', false );
// print_r($pmeta);
$all_swim_lnk=get_post_type_archive_link( 'product' );
?>
			<div class="main-slider main-slider_lg slider-pro" id="main-slider" data-slider-width="100%" data-slider-height="950px" data-slider-arrows="true" data-slider-buttons="true">
				<div class="sp-slides">
					<?php if (!empty($pmeta[$options['prfx'].'mainslider'][0])):
						$slides=maybe_unserialize( $pmeta[$options['prfx'].'mainslider'][0] );
							foreach ($slides as $s) {
								$img=(!empty($s['image_id']))?remove_width_attribute(wp_get_attachment_image($s['image_id'], 'main-slide-img', false, array('class'=>'sp-image'))):'<img class="sp-image" src="'.$options['tpld'].'/assets/img/components/b-main-slider/bg-2.jpg" alt="slider" />';
								printf('<div class="sp-slide">%s<div class="container"><div class="row"><div class="col-sm-10 col-sm-offset-1">%s</div></div></div></div>',
									$img,
									$s['descr']
								);
							}
						?>
					<?php else: ?>
					<div class="sp-slide"><img class="sp-image" src="<?=$options['tpld']?>/assets/img/components/b-main-slider/bg-2.jpg" alt="slider" />
						<div class="container">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-1">
									<div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">Welcome to</div>
									<h2 class="main-slider__title sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400">giammetti</h2>
									<p>There will be the name of the collection</p>
								</div>
							</div>
						</div>
					</div>
					<div class="sp-slide"><img class="sp-image" src="<?=$options['tpld']?>/assets/img/components/b-main-slider/bg-1.jpg" alt="slider" />
						<div class="container">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-1">
									<div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">Welcome to </div>
									<h2 class="main-slider__title sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="800" data-show-delay="400" data-hide-delay="400">giammetti</h2>
									<p>There will be the name of the collection</p>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<section id="collections" class="our_collections">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-5">
							<h2 class="ui-title-block" id="revealfx1"><?=__( 'Collection of swimsuits', 'giammetti' )?></h2>
							<div class="ui-subtitle-block"><?=__( 'OUR COLLECTIONS', 'giammetti' )?></div>
						</div>
						<div class="col-lg-6 col-md-7">
							<?=(!empty($pmeta[$options['prfx'].'collections_descr'][0])) ? $pmeta[$options['prfx'].'collections_descr'][0] : '<p>Our young designers create a unique collection, suitable for confident sexy girls. If you\'re one of these – feel free to choose a swimsuit by GIAMMETTI . Our young designers create a unique collection, suitable for confident sexy girls. If you\'re one of these – feel free to choose a swimsuit by GIAMMETTI .</p>'?>
						</div>
					</div>
				</div>
				<div class="container-fluid nopad">
					<div class="owl-carousel owl-theme enable-owl-carousel" data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="7000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true" data-items="1">
						<?php
						$args=array(
							'taxonomy'=>'product_cat',
							'hide_empty' => false,
						);
						// print_r(get_terms( $args ));
						$collections=array_chunk(get_terms( $args ), 2);
						foreach ($collections as $values) {
							echo '<div class="b-advantages-group"><ul class="list-unstyled js-zoom-gallery">';
							foreach ($values as $val) {
								$img=(!empty(get_term_meta($val->term_id, $options['prfx'].'collection_img_id', true))) ? remove_width_attribute(wp_get_attachment_image(get_term_meta($val->term_id, $options['prfx'].'main-collection_img_id', true), 'main-coll-img', false, array('alt'=>$val->name))) : '<img src="'.$options['tpld'].'/assets/img/collection1.jpg" alt="'.$val->name.'" />';
								printf('<li class="grid-item design illustration"><a class="b-isotope-grid__inner" href="%s">%s<span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">%s</span><span class="b-isotope-grid__categorie">%s</span></span></span></a></li>',
									// $all_swim_lnk.'?collection='.$val->term_id,
									get_term_link($val->term_id),
									$img,
									$val->name,
									__( 'More', 'giammetti' )
								);
							}
							echo '</ul></div>';
						}
						?>
					</div>
				</div>
			</section>
			<section id="swimsuits" class="section-isotope b-isotope swimsuits">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-5 col-xs-12">
							<h2 class="ui-title-block" id="revealfx2"><?=__( 'Collection of swimsuits', 'giammetti' )?></h2>
							<div class="ui-subtitle-block"><?=__( 'Swimsuits', 'giammetti' )?></span>
							</div>
						</div>
						<div class="col-lg-6 col-md-7 col-xs-12 swim_but">
							<a href="<?=$all_swim_lnk?>" class="main_a"><?=__( 'All Swimsuits', 'giammetti' )?></a>
						</div>
					</div>
				</div>
				<?php $swimarr=(!empty($pmeta[$options['prfx'].'attached_swim'][0]))?maybe_unserialize($pmeta[$options['prfx'].'attached_swim'][0]):null;
				$excl=preg_replace('/[\[\]\"\s=]/', '', json_encode( $swimarr ));

				?>
				<div class="container-fluid nopad">
					<ul id="swimsuitsContainer" class="b-isotope-grid grid list-unstyled js-swimsuitsCont" data-page="1"<?=(!empty($excl))?' data-excl="'.$excl.'"':null?>>
						<li class="grid-sizer"></li>
						<li class="gutter-sizer"></li>
						<?php
						$args=array(
							'post_type'=>'product'
						);
						if(empty($swimarr)){
							$args['orderby']='date';
							// $args['order']='asc';
							$args['order']='desc';
							$args['posts_per_page']=4;
						} else {
							$args['post__in']=$swimarr;
							$args['orderby']='post__in';
						}
						$swim=new WP_Query($args);
						if ($swim->have_posts()): while ($swim->have_posts()) : $swim->the_post();
							get_template_part('loop', 'swim_grid-el');
						endwhile;
						else : ?>
						<article>
							<h1><?php _e( 'Sorry, nothing to display.', 'giammetti' ); ?></h1>
						</article>
						<?php endif; ?>
					</ul>
					<p class="center_p">
						<a id="mainMoreSwimsuits" class="btn btn-type-1" href="#"><span class="btn-type-1__inner"><?=__( 'more Swimsuits', 'giammetti' )?></span></a>
					</p>
				</div>
			</section>
			<section id="about" class="section-news section-default">
				<div class="area-bg__inner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="ui-title-block" id="revealfx3"><?=__( 'About Giammetti', 'giammetti' )?></h2>
								<div class="ui-subtitle-block"><?=__( 'About us', 'giammetti' )?></div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-4 about">
								<section class="b-post-1 b-post clearfix">
									<div class="entry-media">
										<?php if (!empty($pmeta[$options['prfx'].'about_img_id'][0])): ?>
											<?=remove_width_attribute(wp_get_attachment_image($pmeta[$options['prfx'].'about_img_id'][0], 'main-about-img-lg', false, array('class'=>'img-responsive')))?>
										<?php else: ?>
										<img class="img-responsive" src="<?=$options['tpld']?>/assets/img/about.jpg" alt="Foto" />
										<?php endif; ?>
									</div>
								</section>
							</div>
							<?php
							$about_cont=(!empty($pmeta[$options['prfx'].'aboutus'][0]))?maybe_unserialize( $pmeta[$options['prfx'].'aboutus'][0] ):null;
							if(!empty($about_cont)){ ?>
							<div class="col-lg-6 col-md-8">
								<div class="owl-carousel owl-theme about-carousel js-about-carousel" data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="70000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true">
								<?php foreach ($about_cont as $val) {
									// print_r($val);
									$ttl=(!empty($val['title'])&&!empty($val['title_lnk'])) ? '<a href="'.$val['title_lnk'].'" target="_blank">'.$val['title'].'</a>':$val['title'];
									$evt1_lnk=(!empty($val['lnk_evt1']))?$val['lnk_evt1']:'#';
									$evt1_img=(!empty($val['img_evt1_id']))?remove_width_attribute(wp_get_attachment_image($val['img_evt1_id'], 'main-about-img', false, array('class'=>'section-form-1__img'))):'<img src="'.$options['tpld'].'/assets/img/about2.jpg" alt="foto" />';
									$evt1_ttl=(!empty($val['title_evt1']))?$val['title_evt1']:null;
									$evt1=(!empty($evt1_ttl)) ? '<li class="grid-item design illustration"><a class="b-isotope-grid__inner" href="'.$evt1_lnk.'">'.$evt1_img.'<span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">'.$evt1_ttl.'</span><span class="b-isotope-grid__categorie">'.__( 'Read more', 'giammetti' ).'</span></span></span></a></li>' : null;
									$evt2_lnk=(!empty($val['lnk_evt2']))?$val['lnk_evt2']:'#';
									$evt2_img=(!empty($val['img_evt2_id']))?remove_width_attribute(wp_get_attachment_image($val['img_evt2_id'], 'main-about-img', false, array('class'=>'section-form-1__img'))):'<img src="'.$options['tpld'].'/assets/img/about2.jpg" alt="foto" />';
									$evt2_ttl=(!empty($val['title_evt2']))?$val['title_evt2']:null;
									$evt2=(!empty($evt2_ttl)) ? '<li class="grid-item design illustration"><a class="b-isotope-grid__inner" href="'.$evt2_lnk.'">'.$evt2_img.'<span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">'.$evt2_ttl.'</span><span class="b-isotope-grid__categorie">'.__( 'Read more', 'giammetti' ).'</span></span></span></a></li>' : null;
									// printf('<div class="b-advantages-group js-adv-holder"><div class="entry-header"><h2 class="entry-title">%s</h2></div><div class="entry_content">%s</div><ul class="b-isotope-grid grid list-unstyled"><li class="grid-sizer"></li><li class="gutter-sizer"></li>%s%s</ul></div>',
									// 	$ttl,
									// 	$val['content'],
									// 	$evt1,
									// 	$evt2
									// );
									printf('<div class="b-advantages-group"><div class="entry-header"><h2 class="entry-title">%s</h2></div><div class="entry_content">%s</div><ul class="b-isotope-grid list-unstyled">%s%s</ul></div>',
										$ttl,
										$val['content'],
										$evt1,
										$evt2
									);
								} ?>
								</div>
							</div>
							<div class="col-xs-12"><div class="about-nav js-about-nav"></div></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<section id="instagram" class="instagram">
				<section class="section-isotope b-isotope swimsuits">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-5">
								<h2 class="ui-title-block" id="revealfx4"><?=__( 'Collection of swimsuits', 'giammetti' )?></h2>
								<div class="ui-subtitle-block"><?=__( 'INSTAGRAM', 'giammetti' )?></div>
							</div>
							<div class="col-lg-6 col-md-7 col-xs-12 swim_but">
								<a href="<?=(!empty($options['socl']['instagram']))?$options['socl']['instagram']:'#'?>" class="main_a"><?=__( 'Go to page', 'giammetti' )?></a>
							</div>
						</div>
					</div>
					<?php fetchInstaData(); ?>
				</section>
			</section>
			<section id="contact" class="section-form-1">
				<div class="block-table block-table_sm ">
					<div class="container">
						<div class="block-table__cell col-md-6">
							<div class="section-form-1__inner">
								<h2 class="ui-title-block"><?=__( 'Get In Touch !', 'giammetti' )?></h2>
								<div class="ui-subtitle-block"><?=__( 'Contact us', 'giammetti' )?></div>
								<div class="section-form-1__description">
									<div class="row">
										<div class="col-md-6">
											<p><strong><?=(!empty($options['socl']['phone1'])) ? '<a href="tel:'.preg_replace("/[^A-Za-z0-9+]/", '', $options['socl']['phone1']).'"> '.$options['socl']['phone1'].'</a>':null?></strong></p>
										</div>
										<div class="col-md-6">
											<p><strong><?=(!empty($options['socl']['email'])) ? '<a href="mailto:'.$options['socl']['email'].'"> '.$options['socl']['email'].'</a>':null?></strong></p>
										</div>
									</div>
								</div>
								<?php if (!empty($pmeta[$options['prfx'].'contact_sc'][0])): ?>
									<?=do_shortcode( $pmeta[$options['prfx'].'contact_sc'][0] )?>
								<?php else: ?>
								<form class="ui-form ui-form-1" action="#" method="post">
									<div class="row">
										<div class="col-xs-12">
											<input class="form-control" type="text" placeholder="Your Name">
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<input class="form-control" type="url" placeholder="Email address">
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<textarea class="form-control" rows="6" placeholder="Message Body"></textarea>
											<button class="btn btn-type-1"><span class="btn-type-1__inner">send message</span></button>
										</div>
									</div>
								</form>
								<?php endif; ?>
							</div>
						</div>
						<div class="block-table__cell col-md-6">
							<div class="block-table__inner">
								<?php if (!empty($pmeta[$options['prfx'].'contact_img_id'][0])) {
									echo remove_width_attribute(wp_get_attachment_image($pmeta[$options['prfx'].'contact_img_id'][0], 'main-contact-img', false, array('class'=>'section-form-1__img')));
								} else { ?>
								<img class="section-form-1__img" src="<?=$options['tpld']?>/assets/img/footer.jpg" alt="foto">
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>

<?php get_footer(); ?>
