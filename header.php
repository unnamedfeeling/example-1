<?php
global $options;
$class='';
$class.=(is_tax(array('collection', 'product_cat'))) ? ' col_wrapper' : null;
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title('', true); ?></title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-57x57.png?v=4">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-60x60.png?v=4">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-72x72.png?v=4">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-76x76.png?v=4">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-114x114.png?v=4">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-120x120.png?v=4">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-144x144.png?v=4">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-152x152.png?v=4">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=$options['tpld']?>/assets/img/icon/apple-touch-icon-180x180.png?v=4">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=$options['tpld']?>/assets/img/icon/favicon.png?v=4">
		<link rel="icon" type="image/png" sizes="194x194" href="<?=$options['tpld']?>/assets/img/icon/favicon-194x194.png?v=4">
		<link rel="icon" type="image/png" sizes="192x192" href="<?=$options['tpld']?>/assets/img/icon/android-chrome-192x192.png?v=4">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=$options['tpld']?>/assets/img/icon/favicon.png?v=4">
		<link rel="manifest" href="<?=$options['tpld']?>/assets/img/icon/manifest.json?v=4">
		<link rel="mask-icon" href="<?=$options['tpld']?>/assets/img/icon/safari-pinned-tab.svg?v=4" color="#000000">
		<link rel="shortcut icon" href="<?=$options['tpld']?>/assets/img/icon/favicon.ico?v=4">
		<meta name="msapplication-TileColor" content="#2b5797">
		<meta name="msapplication-TileImage" content="/assets/img/icon/mstile-144x144.png?v=4">
		<meta name="msapplication-config" content="/assets/img/icon/browserconfig.xml?v=4">
		<meta name="theme-color" content="#000000">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head(); ?>
		<!--[if lt IE 9 ]>
		<script src="//cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js" type="text/javascript"></script>
		<meta content="no" http-equiv="imagetoolbar">
		<![endif]-->
		<style>@-webkit-keyframes loaderRotate{0%,50%{-webkit-transform:translateY(0);transform:translateY(0)}30%{-webkit-transform:translateY(-.75em);transform:translateY(-.75em)}70%{-webkit-transform:translateY(.75em);transform:translateY(.75em)}}@keyframes loaderRotate{0%,50%{-webkit-transform:translateY(0);transform:translateY(0)}30%{-webkit-transform:translateY(-.75em);transform:translateY(-.75em)}70%{-webkit-transform:translateY(.75em);transform:translateY(.75em)}}.loading{overflow:hidden}.loading .loader-wrapper{position:fixed;top:0;right:0;bottom:0;left:0;background:#fff;z-index:9999}.loading .loader{text-align:center;position:absolute;width:100%;top:calc(50% - 19px);top:calc(50vh - 19px)}.loading .loader .inner1,.loading .loader .inner2,.loading .loader .inner3{display:inline-block;margin:.375em;width:1.5em;height:1.5em;border:1px solid #000;background-color:#000;-webkit-transform-origin:50%;-webkit-animation-duration:.75s;-webkit-animation-name:loaderRotate;-webkit-animation-iteration-count:infinite;-webkit-animation-timing-function:linear;transform-origin:50%;animation-duration:.75s;animation-name:loaderRotate;animation-iteration-count:infinite;animation-timing-function:linear}.loading .loader .inner2{-webkit-animation-delay:.1875s}.loading .loader .inner3{-webkit-animation-delay:.375s}</style>
	</head>
	<body <?php body_class( 'loading' ) ?>>
		<div class="js-preloader loader-wrapper"><div class="loader"><span class="inner1"></span><span class="inner2"></span><span class="inner3"></span></div></div>
		<header class="header header_mod-a header-topbar-hidden header-boxed-width navbar-fixed-top header-background-trans header-color-white header-logo-white header-navibox-1-left header-navibox-2-right">
			<div class="container container-boxed-width">
				<nav class="navbar" id="nav">
					<div class="header-navibox-1">
						<button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
						<?php
						$logo=(empty($options['gnrl']['header_logo_id'])) ? '<img src="'.$options['tpld'].'/assets/img/general/logo.png" alt="'.get_option( 'blogname' ).'" class="normal-logo">' : remove_width_attribute(wp_get_attachment_image( $options['gnrl']['header_logo_id'], 'full', false, array('class'=>'normal-logo', 'alt'=>get_option( 'blogname', false )) ));
						$logo.=(empty($options['gnrl']['header_logo_scroll_id'])) ? '<img src="'.$options['tpld'].'/assets/img/general/logo-dark.png" alt="'.get_option( 'blogname' ).'" class="scroll-logo hidden-xs">' : remove_width_attribute(wp_get_attachment_image( $options['gnrl']['header_logo_scroll_id'], 'full', false, array('class'=>'scroll-logo hidden-xs', 'alt'=>get_option( 'blogname', false )) ));
						if(!is_front_page()){ ?>
							<?php if(function_exists('icl_get_languages')){
								$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
							} else {
								$home_url='/';
							} ?>
							<a class="navbar-brand scroll" href="<?=$home_url?>">
								<?= $logo ?>
							</a>
						<?php } else { ?>
							<?= '<a class="navbar-brand scroll" href="#">'.$logo.'</a>' ?>
						<?php } ?>
					</div>
					<div class="header-navibox-2">
						<?php main_nav(); ?>
					</div>
					<ul class="s-links list-inline">
						<li class="topheadmenu_item">
							<a href="/cart" class="topheadmenu_link" id="minicart" target="_blank">
								<i class="fa fa-shopping-cart"></i>
								<span class="js-cart-total">0</span>
							</a>
						</li>
						<li><a href="<?=(!empty($options['socl']['facebook']))?$options['socl']['facebook']:'https://www.facebook.com/giammettivienna/'?>" target="_blank"><i class="s_icons fa fa-facebook"></i></a></li>
						<li><a href="<?=(!empty($options['socl']['instagram']))?$options['socl']['instagram']:'https://www.instagram.com/giammettivienna/'?>" target="_blank"><i class="s_icons fa fa-instagram"></i></a></li>
						<li class="li-last"><a href="<?=(!empty($options['socl']['vk']))?$options['socl']['vk']:'https://www.vk.com/giammettivienna/'?>" target="_blank"><i class="s_icons fa fa-vk"></i></a></li>
						<?php if(function_exists('icl_get_languages')){
							$langs=icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
							if(count($langs)>1){
								$list='';
								foreach ($langs as $l) {
									$list.='<li><a href="'.((!$l['active']) ? $l['url'] : '#').'"'.(($l['active'])?' style="color:#d9b273"':null).'>'.mb_substr($l['translated_name'], 0, 2).'</a></li>';
								}
								echo $list;
							}
						} else { ?>
						<li><a href="#">En</a></li>
						<li><a href="#">De</a></li>
						<?php } ?>
					</ul>
				</nav>
			</div>
		</header>
		<div class="l-theme animated-css" data-header="sticky" data-header-top="200" canvas="container">
			<main class="l-main-content<?=$class?>">
