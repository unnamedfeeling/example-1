<?php
global $options, $product;
$swmeta=get_post_meta( $post->ID, '', $single = false ) ?>
<li class="b-isotope-grid__item grid-item design">
	<a class="b-isotope-grid__inner" href="<?=get_the_permalink()?>">
		<?php if (has_post_thumbnail()) {
			echo remove_width_attribute(wp_get_attachment_image(get_post_thumbnail_id(), 'all-swim-img', false, array('alt'=>$post->post_title)));
		} else { ?>
		<img src="<?=$options['tpld']?>/assets/img/all_swim/1.jpg" alt="<?=$post->post_title?>" />
		<?php } ?>
		<span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical">
			<span class="b-isotope-grid__info">
				<span class="b-isotope-grid__title"><?=$post->post_title?></span>
				<span class="b-isotope-grid__categorie"><?=$product->get_price_html()?></span>
			</span>
		</span>
	</a>
</li>
