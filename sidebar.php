<aside class="col-md-3">
	<div class="row">
		<div class="collection_wrap col-md-12 col-xs-6">
			<div class="ui-decor-1"></div>
			<h3 class="widget-title ui-title-inner"><?=__( 'Collections', 'giammetti' )?></h3>
			<ul class="widget-list list-unstyled">
				<?php
				$args=array(
					'taxonomy' => 'product_cat',
					// 'hide_empty' => false
				);
				$cats=get_terms($args);
				foreach ($cats as $cat) {
					printf('<li class="widget-list__item"><a class="widget-list__link js-filterCat" href="%s" data-catid="%s" data-tax="product_cat">%s</a></li>',
						get_term_link( $cat, 'product_cat'),
						$cat->term_id,
						$cat->name
					);
				}
				?>
			</ul>
		</div>
		<div class="collection_wrap col-md-12 col-xs-6">
			<div class="ui-decor-1"></div>
			<h3 class="widget-title ui-title-inner"><?=__( 'Style', 'giammetti' )?></h3>
			<ul class="widget-list list-unstyled">
				<?php
				$args=array(
					'taxonomy' => 'product_tag',
					// 'hide_empty' => false
				);
				$cats=get_terms($args);
				foreach ($cats as $cat) {
					printf('<li class="widget-list__item"><a class="widget-list__link js-filterTag" href="%s" data-tagid="%s" data-tagslug="%s" data-tax="product_tag">%s</a></li>',
						get_term_link( $cat, 'product_tag'),
						$cat->term_id,
						$cat->slug,
						$cat->name
					);
				}
				?>
			</ul>
		</div>
	</div>
</aside>
