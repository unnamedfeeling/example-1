<?php
/**
 * Author: Todd Motto | @toddmotto
 * URL: html5blank.com | @html5blank
 * Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// CMB2
if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/cmb2/init.php';
} elseif ( file_exists(  dirname( __FILE__ ) . '/assets/php/CMB2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/CMB2/init.php';
}
// CMB2 attached posts
if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php';
}

// metaboxes include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/metaboxes.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/metaboxes.php';
}

// tabbed admin menu include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/tabbed-admin-menu.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/tabbed-admin-menu.php';
}

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width)){
	$content_width = 900;
}

if (function_exists('add_theme_support')){

	// Add Thumbnail Theme Support
	add_theme_support('post-thumbnails');
	add_image_size('large', 700, '', true); // Large Thumbnail
	add_image_size('medium', 250, '', true); // Medium Thumbnail
	add_image_size('small', 120, '', true); // Small Thumbnail
	add_image_size('main-slide-img', 9999, 950, true);
	add_image_size('main-about-img-lg', 470, 555, true);
	add_image_size('main-about-img', 310, 249, true);
	add_image_size('main-contact-img', 618, 661, true);
	add_image_size('main-swim-grid-img', 460, 582, true);
	add_image_size('main-coll-img', 951, 546, true);
	add_image_size('all-swim-img', 270, 342, true);
	add_image_size('cat-catimg', 555, 310, true);
	add_image_size('cat-moodboard', 430, 9999, true);
	add_image_size('cat-galery-sm', 430, 260, true);
	add_image_size('cat-galery-md', 430, 550, true);
	add_image_size('cat-galery-lg', 910, 260, true);
	add_image_size('product-xs', 106, 112, true);
	add_image_size('product-sm', 212, 224, true);
	add_image_size('product-md', 318, 336, true);
	add_image_size('product-lg', 670, 710, true);
	add_image_size('product-fl', 1340, 1420, true);

	// Add Support for Custom Backgrounds - Uncomment below if you're going to use
	/*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
	));*/

	// Add Support for Custom Header - Uncomment below if you're going to use
	/*add_theme_support('custom-header', array(
	'default-image'		  => get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'	 => '000',
	'width'				  => 1000,
	'height'				 => 198,
	'random-default'		 => false,
	'wp-head-callback'	   => $wphead_cb,
	'admin-head-callback'	=> $adminhead_cb,
	'admin-preview-callback' => $adminpreview_cb
	));*/

	// Enables post and comment RSS feed links to head
	add_theme_support('automatic-feed-links');

	// Enable HTML5 support
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

	// Localisation Support
	load_theme_textdomain('giammetti', get_template_directory() . '/assets/languages');

	add_theme_support( 'woocommerce' );
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Global options Define
function generic_globals(){
	global $options;
	$options['brnc']=get_option('generic_options');
	$options['gnrl']=get_option('general_options');
	$options['socl']=get_option('social_options');
	$options['advd']=get_option('advanced_options');
	$options['tpld']=get_template_directory_uri();
	$options['prfx']='giam_';
	$options['translate'] = array(
		"am" => "дп",
		"pm" => "пп",
		"AM" => "ДП",
		"PM" => "ПП",
		"Monday" => "Понедельник",
		"Mon" => "пн",
		"Tuesday" => "Вторник",
		"Tue" => "вт",
		"Wednesday" => "Среда",
		"Wed" => "ср",
		"Thursday" => "Четверг",
		"Thu" => "чт",
		"Friday" => "Пятница",
		"Fri" => "пт",
		"Saturday" => "Суббота",
		"Sat" => "сб",
		"Sunday" => "Воскресенье",
		"Sun" => "вс",
		"January" => "Января",
		"Jan" => "янв",
		"February" => "Февраля",
		"Feb" => "фев",
		"March" => "Марта",
		"Mar" => "мар",
		"April" => "Апреля",
		"Apr" => "апр",
		"May" => "мая",
		"June" => "Июня",
		"Jun" => "июн",
		"July" => "Июля",
		"Jul" => "июл",
		"August" => "Августа",
		"Aug" => "авг",
		"September" => "Сентября",
		"Sep" => "сен",
		"October" => "Октября",
		"Oct" => "окт",
		"November" => "Ноября",
		"Nov" => "ноя",
		"December" => "Декабря",
		"Dec" => "дек",
		"st" => "ое",
		"nd" => "ое",
		"rd" => "е",
		"th" => "ое"
	);
}

// HTML5 Blank navigation
function main_nav(){
	$loc=(is_front_page()) ? 'header-menu' : 'header-menu-nh';
	wp_nav_menu(
	array(
		'theme_location'	=> $loc,
		'menu'				=> '',
		'container'	  		=> 'div',
		'container_class'	=> 'menu-{menu slug}-container',
		'container_id'		=> '',
		'menu_class'		=> 'menu',
		'menu_id'		 	=> '',
		'echo'				=> true,
		'fallback_cb'	 	=> 'wp_page_menu',
		'before'			=> '',
		'after'				=> '',
		'link_before'		=> '',
		'link_after'		=> '',
		'items_wrap'		=> '<ul class="yamm main-menu nav navbar-nav">%3$s</ul>',
		'depth'				=> 0,
		'walker'			=> new Nav_Menu_Walker
		)
	);
}

// Menu walker
class Nav_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth	 Depth of menu item. May be used for padding.
	 * @param  array $args	Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		global $post;
		// $class=($depth>=1) ? 'header__bottom-sub-menu-item' : 'header__bottom-menu-item';
		$class='';
		$class.=($depth==0&&$item->current) ? ' active':'';
		// $class.=($depth>=1 ? 'header__bottom-sub-menu-link ' : 'header__bottom-menu-link ');
		$class.=($depth>=1&&$item->current ? ' active ':'');
		$class.=(in_array('menu-item-has-children', $item->classes)) ? ' dropdown active' : null;
		$output.= (!empty($class)) ? '<li class="'.$class.'">' : '<li>';
		// $jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-drop-btn' : '';
		// $attributes  = ($depth>=1) ? ' class="header__bottom-sub-menu-link"' : ' class="header__bottom-menu-link'.$jsbtn.'"';
		$attributes = (in_array('menu-item-has-children', $item->classes)) ? ' class="dropdown-toggle"' : null;
		// $attributes='';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title	   = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		$url=((!is_front_page()&&$depth==0&&$item->url[0]=='#') ? '/' : null).$item->url;
		! empty ( $url )
			and $attributes .= ' href="' . esc_attr( $url ) .'"';
		$attributes  = trim( $attributes );
		$title	   = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="dropdown-menu">';
		} else {
			$output .= '<ul>';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		// $output .= '</ul>';
		if($depth>=0){
			$output .= '</ul>';
		} else {
			$output .= '</li>';
		}
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

function generic_footer_nav(){
	wp_nav_menu(
	array(
		'theme_location'  => 'footer-menu',
		'menu'			=> '',
		'container'	   => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'	=> '',
		'menu_class'	  => 'menu',
		'menu_id'		 => '',
		'echo'			=> true,
		'fallback_cb'	 => 'wp_page_menu',
		'before'		  => '',
		'after'		   => '',
		'link_before'	 => '',
		'link_after'	  => '',
		'items_wrap'	  => '<ul class="footer-list list-unstyled">%3$s</ul>',
		'depth'		   => 0,
		'walker'		  => new Nav_Footer_Menu_Walker
		)
	);
}

// Menu walker
class Nav_Footer_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth	 Depth of menu item. May be used for padding.
	 * @param  array $args	Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		// $jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-footer-submenu' : '';
		$class='footer-list__item';
		// $class.=($depth==0&&$item->current) ? ' active':'';
		// $class.=($depth>=1 ? 'footer-main__link ':'');
		// $class.=($depth>=1&&$item->current ? 'active ':'');
		// $class.=' d-'.$depth;
		$output.= '<li class="'.$class.'">';
		$attributes  = ($depth>=1) ? '' : ' class="footer-list__link"';
		// $attributes  = '';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title	   = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title	   = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="footer-main-sublist">';
		} else {
			$output .= '<ul class="footer-list list-unstyled">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		$output .= '</ul>';
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

// Load HTML5 Blank scripts (header.php)
function generic_header_scripts(){
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );
		// wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3/dist/jquery.min.js,npm/jquery-migrate@3/dist/jquery-migrate.min.js,npm/owl.carousel@2/dist/owl.carousel.min.js,npm/bootstrap@3/dist/js/bootstrap.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/typed@0.3.3/src/typed.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js', array(), '3.2.1');
		// wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3/dist/jquery.min.js,npm/jquery-migrate@3/dist/jquery-migrate.min.js,npm/owl.carousel@2/dist/owl.carousel.min.js,npm/bootstrap@3/dist/js/bootstrap.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js', array(), '3.2.1');
		// wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3.2.1/dist/jquery.min.js,npm/jquery-migrate@3.0.1/dist/jquery-migrate.min.js,npm/bootstrap@3/dist/js/bootstrap.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js,npm/guillotine@1.3.1,npm/slidebars@2.0.2/dist/slidebars.min.js,npm/lightgallery@1.6.4/dist/js/lightgallery-all.min.js', array(), '3.2.1');
		// wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3.2.1/dist/jquery.min.js,npm/jquery-migrate@3.0.1/dist/jquery-migrate.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js,npm/guillotine@1.3.1,npm/slidebars@2.0.2/dist/slidebars.min.js,npm/lightgallery@1.6.4/dist/js/lightgallery-all.min.js', array(), '3.2.1');
		wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3.2.1/dist/jquery.min.js,npm/jquery-migrate@3.0.1/dist/jquery-migrate.min.js,npm/owl.carousel@2/dist/owl.carousel.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js,npm/guillotine@1.3.1,npm/slidebars@2.0.2/dist/slidebars.min.js,npm/lightgallery@1.6.5/dist/js/lightgallery-all.min.js', array(), '3.2.1');
		// wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3.2.1/dist/jquery.min.js,npm/jquery-migrate@3.0.1/dist/jquery-migrate.min.js,npm/owl.carousel@2/dist/owl.carousel.min.js,npm/magnific-popup@1/dist/jquery.magnific-popup.min.js,npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/waypoints@4/lib/jquery.waypoints.min.js,npm/slider-pro@1/dist/js/jquery.sliderPro.min.js,npm/sticky-kit@1/dist/sticky-kit.min.js,npm/imagesloaded@4/imagesloaded.pkgd.min.js,npm/animejs@2/anime.min.js,npm/guillotine@1.3.1,npm/slidebars@2.0.2/dist/slidebars.min.js,npm/lightgallery@1.6.4/dist/js/lightgallery-all.min.js,npm/owl.carousel2.thumbs@0.1.8', array(), '3.2.1');
	}
}

// Load HTML5 Blank conditional scripts
function generic_conditional_scripts(){
	if (is_page('pagenamehere')) {
		// Conditional script(s)
		wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0');
		wp_enqueue_script('scriptname');
	}
}

// Load HTML5 Blank styles
function generic_styles(){
	// global $post;
	// print_r($GLOBALS);
	wp_enqueue_style('gfonts', '//fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Montserrat:400,700|Vidaloka|Allura', array(), '1.0', 'all');
	wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '1.0', 'all');
	wp_enqueue_style('bundle-css', get_template_directory_uri().'/assets/css/bundle.css', array(), '1.0', 'all');
}

// footer scripts
function generic_footer_scripts(){
	global $options;
	wp_deregister_script( 'wp-embed' );
	// wp_enqueue_script('owl-carousel', $options['tpld'].'/assets/js/libs/owl-carousel/owl.carousel.min.js', array('jquery'), '1.0');
	// wp_enqueue_script('owl-carousel', $options['tpld'].'/assets/js/libs/owl-carousel2/owl.carousel.min.js', array('jquery'), '1.0');
	// wp_enqueue_script('fitcolumns', $options['tpld'].'/assets/js/libs/isotope/fitcolumns.min.js', array('jquery'), '1.0');
	wp_enqueue_script('reveal-js', $options['tpld'].'/assets/js/libs/revealer/js/revealer.min.js', array('jquery'), '1.0');
	// wp_enqueue_script('slickslider', get_template_directory_uri() . '/assets/js/libs/slick/slick/slick.min.js', array('jquery'), '1.6.1');
	wp_enqueue_script('genericscripts-min', get_template_directory_uri() . '/assets/js/scripts.min.js', array('jquery', 'reveal-js'), '1.0.0');
	wp_localize_script( 'genericscripts-min', 'ajax_func', array(
		'nonce'	=> wp_create_nonce( 'ultimate-nonce' ),
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
}

//cleanup version tag
function remove_cssjs_ver( $src ) {
	$src = remove_query_arg( array('v', 'ver', 'rev', 'id'), $src );
	return $src;
}

// remove emoji
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  // add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

// Register HTML5 Blank Navigation
function register_generic_menu(){
	register_nav_menus(array( // Using array to specify more menus if needed
		'header-menu' => __('Main menu', 'giammetti'), // Main Navigation
		'header-menu-nh' => __('Main menu (not home page)', 'giammetti'), // Main Navigation
		'footer-menu' => __('Меню футера', 'giammetti'), // Sidebar Navigation
		// 'extra-menu' => __('Extra Menu', 'giammetti') // Extra Navigation if needed (duplicate as many as you need!)
	));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = ''){
	$args['container'] = false;
	return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var){
	return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist){
	return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes){
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}

// Remove the width and height attributes from inserted images
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')){
	// Define Sidebar Widget Area 1
	register_sidebar(array(
		'name' => __('Widget Area 1', 'giammetti'),
		'description' => __('Description for this widget-area...', 'giammetti'),
		'id' => 'widget-area-1',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

	// Define Sidebar Widget Area 2
	register_sidebar(array(
		'name' => __('Widget Area 2', 'giammetti'),
		'description' => __('Description for this widget-area...', 'giammetti'),
		'id' => 'widget-area-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style(){
	global $wp_widget_factory;

	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action('wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		));
	}
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination(){
	global $wp_query;
	$big = 999999999;
	echo paginate_links(array(
		'base' => str_replace($big, '%#%', get_pagenum_link($big)),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'total' => $wp_query->max_num_pages
	));
}

// set custom number for 'posts_per_page' per posttype
function posts_per_page_func( $query ) {
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'project' ) ) {
		$query->set( 'posts_per_page', '100' );
	}
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'post' ) ) {
	// if ( !is_admin() && $query->is_main_query() && is_page( 'novosti' ) ) {
		// $query->set( 'posts_per_page', '-1' );
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'asc' );
	}
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
	return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length){
	return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = ''){
	global $post;
	if (function_exists($length_callback)) {
		add_filter('excerpt_length', $length_callback);
	}
	if (function_exists($more_callback)) {
		add_filter('excerpt_more', $more_callback);
	}
	$output = get_the_excerpt();
	$output = apply_filters('wptexturize', $output);
	$output = apply_filters('convert_chars', $output);
	$output = '<p>' . $output . '</p>';
	echo $output;
}

// Custom View Article link to Post
function generic_blank_view_article($more){
	global $post;
	return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'giammetti') . '</a>';
}

// Remove Admin bar
function remove_admin_bar(){
	return false;
}

// Remove 'text/css' from our enqueued stylesheet
function generic_style_remove($tag){
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ){
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults){
	$myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[$myavatar] = "Custom Gravatar";
	return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script('comment-reply');
		}
	}
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth){
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
	<!-- heads up: starting < for the html tag (li or div) in the next line: -->
	<<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

// likes function
function generic_news_like() {
	// global $post;
	$id=$_POST['params']['like_id'];
	$liked=0;
	$exp=time()+60*60*24*30;
	// if($_COOKIE['liked']!==$id){
		// exit(json_encode($_POST));
		$love = get_post_meta( $id, 'generic_post_likes', true );
		$love++;
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			update_post_meta( $id, 'generic_post_likes', $love );
			echo $love;
			$liked=1;
		}
		// setcookie('liked', $id, $exp);
	// }
	$response=[
		'id' => $id,
		'time' => $exp,
		'liked' => $liked
	];
	die(json_encode($response));
}

// ajax get posts for main page
function generic_ajax_posts(){
	// global $post;
	$params=$_POST;
	// die(json_encode( $params ));
	$offset=$params['params']['p']*((!empty($params['params']['q'])) ? $params['params']['q'] : 4);
	$post_type=$params['params']['pt'];
	$catid=(!empty($params['params']['cat']))?$params['params']['cat']:null;
	$excl=(!empty($params['params']['e']))?explode(',', $params['params']['e']):array();
	$args=array(
		'post_type'=>$post_type,
		'posts_per_page'=>4,
		'offset'=>(empty($excl))?$offset:0,
		'post__not_in'=> $excl
	);
	if(!empty($catid)){
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_cat',
				'terms'=>array($catid)
			)
		);
	}
	$posts=new WP_Query($args);
	ob_start();
	if (!empty($excl)) {
		$ret_ids='';
	}
	if ($posts->have_posts()): while ($posts->have_posts()) : $posts->the_post();
		$ret_ids.=(!empty($excl))?get_the_ID().',':null;
		get_template_part('loop', 'swim_grid-el');
	endwhile;
	else : ?>
		<div class="noposts grid-item design fullwidth" style="width:100%;font-size:2rem;font-weight:bold;"><?=(empty($offset)||$offset==1)?__( 'No swimsuits!', 'giammetti' ):__( 'No more swimsuits!', 'giammetti' )?></div>
	<?php endif;
	// die(json_encode($args));
	$cont=ob_get_clean();
	// $cont=str_replace('data-src', 'src', $cont);
	// $response=[
	// 	'offset' => $offset,
	// 	'total_posts' => wp_count_posts('post'),
	// 	'content' => $cont
	// ];
	if (!empty($excl)) {
		$response['pids']=$ret_ids;
	}
	$response['offset']=$offset;
	$response['total_posts']=wp_count_posts($post_type)->publish;
	$response['content']=$cont;
	// $response['params']=$params;
	die(json_encode($response));
}

// ajax get posts for main page
function generic_ajax_filterPosts(){
	// global $post;
	// wp_reset_query();
	$params=$_POST;
	$post_type=$params['params']['pt'];
	// $catid=(!empty($params['params']['cat']))?$params['params']['cat']:$params['params']['c'];
	// $tagid=(!empty($params['params']['tag']))?$params['params']['tag']:$params['params']['t'];
	$catid=(empty($params['params']['c']))?null:(int) $params['params']['c'];
	$tagid=(empty($params['params']['t']))?null:(int) $params['params']['t'];
	$args=array(
		'post_type'=>$post_type,
		'posts_per_page'=>12,
		'post_status'=>'publish',
		// 'offset'=>0
	);
	if(!empty($catid)){
		$response['tax_type']='cat';
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_cat',
				'field' => 'id',
				'terms'=>array((int) $catid)
			)
		);
	}
	if(!empty($tagid)){
		$response['tax_type']='tag';
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_tag',
				'field' => 'id',
				'terms'=>(int) $tagid
			)
		);
		// $args['product_tag']=$tagid;
	}
	$posts=new WP_Query($args);
	ob_start();
	if ($posts->have_posts()): while ($posts->have_posts()) : $posts->the_post();
		get_template_part('loop', 'swim_grid-gen');
	endwhile;
	else : ?>
		<div class="noposts grid-item design fullwidth" style="width:100%;font-size:2rem;font-weight:bold;"><?=(empty($offset)||$offset==1)?__( 'No swimsuits!', 'giammetti' ):__( 'No more swimsuits!', 'giammetti' )?></div>
	<?php endif;
	// die(json_encode($args));
	$cont=ob_get_clean();
	// $cont=str_replace('data-src', 'src', $cont);
	// $response=[
	// 	'offset' => $offset,
	// 	'total_posts' => wp_count_posts('post'),
	// 	'content' => $cont
	// ];
	$response['offset']=$offset;
	$response['total_posts']=wp_count_posts($post_type)->publish;
	$response['content']=$cont;
	// $response['query']=$posts;
	$response['args']=$args;
	$response['post']=$params;
	$response['type']='generic_ajax_filterPosts';
	die(json_encode($response));
}

// ajax get cart content
function generic_ajax_cart(){
	global $woocommerce;
	ob_start();
	echo WC()->cart->get_cart_contents_count();
	$response['content']=ob_get_clean();
	die(json_encode($response));
}

// ajax add to cart
function generic_ajax_add_to_cart() {
	global $woocommerce;
	$params=$_POST['params'];

	// die(json_encode( $params ));

	$prid = $params['prodid'];
	$qty = $params['quant'];
	$var = (!empty($params['variation'])) ? $params['variation'] : null;
	$pass = apply_filters( 'woocommerce_add_to_cart_validation', true, $prid, $qty );
	$pr_stat = get_post_status( $prid );
	$res=array();
	// $res['id']=$prid;
	// $res['qty']=$qty;
	// $res['pass']=$pass;
	// $res['stat']=$pr_stat;
	$product=new WC_Product($prid);
	// wp_send_json_success( $res );

	if ( $pass && WC()->cart->add_to_cart( $prid, $qty, $var ) && 'publish' === $pr_stat ) {
		// ob_start();
		do_action( 'woocommerce_ajax_added_to_cart', $prid );
		$res['result']=wc_add_to_cart_message( $prid, true, true );
		$ttl='';
		if($var==0||$var==''){
			$ttl=$product->get_name();
		} else {
			$variations=new WC_Product_Variation($var);
			$res['prod']=json_encode( $variations );
			$res['attr']=json_encode($variations->get_attributes());
			$terms='';
			$i=1;
			$c=count($variations->get_attributes());
			foreach ($variations->get_attributes() as $key => $value) {
				// $terms.='$key='.$key.', $value='.$value.', $pa_pos='.strpos($key,'pa_');
				$varname=(strpos($key,'pa_')&&strpos($key,'pa_')>=0)?get_term_by( 'slug', $value, $key )->name : get_term_by( 'slug', $value, 'pa_'.$key )->name;
				$terms.=(!empty($varname))?$varname:get_term_by( 'slug', $value, $key )->name;
				$terms.=($i<$c) ? ', ' : null;
				$i++;
			}

			$res['title']=json_encode($terms);
			$ttl=$product->get_name().' ('.$terms.')';
		}
		// $res['html']='<div id="simpleModal" class="cartmodal"><div class="fade"></div><div class="modal-window small"><button class="close js-close" title="'.__( 'Close', 'giammetti' ).'">&times;</button><h3>'.__( 'Your product has been added to cart!', 'giammetti' ).'</h3><p>'.__( 'Product', 'giammetti' ).' - '.$ttl.', '.$qty.' '.__( 'pcs', 'giammetti' ).'.</p>
		// <p>'.__( 'Go to', 'giammetti' ).' <a href="'.$woocommerce->cart->get_cart_url().'" target="_blank">'.__( 'cart', 'giammetti' ).'</a>?</p><p><a href="'.$woocommerce->cart->get_checkout_url().'" target="_blank">'.__( 'Checkout', 'giammetti' ).'</a>?</p></div></div>';
		$res['html']='<div class="cartmodal-window small"><h3>'.__( 'Your product has been added to cart!', 'giammetti' ).'</h3><p>'.__( 'Product', 'giammetti' ).' - '.$ttl.', '.$qty.' '.__( 'pcs', 'giammetti' ).'.</p>
		<p>'.__( 'Go to', 'giammetti' ).' <a href="'.wc_get_cart_url().'" target="_blank">'.__( 'cart', 'giammetti' ).'</a>?</p><p><a href="'.wc_get_checkout_url().'" target="_blank">'.__( 'Checkout', 'giammetti' ).'</a>?</p></div>';
		wp_send_json_success( $res );
	} else {
		// If there was an error adding to the cart, redirect to the product page to show any errors
		$res['result'] = array(
			'error'	   => true,
			'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $prid ), $prid )
		);
		wp_send_json_error( $res );
	}
	die();
}

// callback for diltering html tags in admin input
function unfilter_html_tags( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization.
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	$value = strip_tags( $value, '<p><a><br><br/><span>' );

	return $value;
}


// callback for diltering all html tags in admin input
function unfilter_all( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization.
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	// $value = strip_tags( $value, '<p><a><br><br/><span><iframe><div>' );
	$value=str_replace('"', '\'', $value);

	return $value;
}

// callback for diltering all html tags in admin input
function unescape_all( $value, $field_args, $field ) {
	$value=str_replace('\"', '\'', $value);
	$value=str_replace('\\\'', '\'', $value);
	return $value;
}

// check if this $post->ID has children
function has_children($pid) {
	$children = get_children( array(
		'post_parent' => $pid,
		'post_type'   => 'any',
		'numberposts' => -1,
		'post_status' => 'publish'
	) );
	if( count( $children ) > 0 ) {
		return true;
	} else {
		return false;
	}
}

function wp_get_attachment_image_lazy($attachment_id, $size = 'thumbnail', $icon = false, $attr = '') {
	$html = '';
	$image = wp_get_attachment_image_src($attachment_id, $size, $icon);
	if ( $image ) {
		list($src, $width, $height) = $image;
		$hwstring = image_hwstring($width, $height);
		$size_class = $size;
		if ( is_array( $size_class ) ) {
			$size_class = join( 'x', $size_class );
		}
		$attachment = get_post($attachment_id);
		$default_attr = array(
			'src'	=> get_template_directory_uri().'/assets/img/transparent.gif',
			'class'	=> "attachment-$size_class size-$size_class",
			'alt'	=> trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ),
		);

		$attr = wp_parse_args( $attr, $default_attr );

		// Generate 'srcset' and 'sizes' if not already present.
		if ( empty( $attr['srcset'] ) ) {
			$image_meta = wp_get_attachment_metadata( $attachment_id );

			if ( is_array( $image_meta ) ) {
				$size_array = array( absint( $width ), absint( $height ) );
				$srcset = wp_calculate_image_srcset( $size_array, $src, $image_meta, $attachment_id );
				$sizes = wp_calculate_image_sizes( $size_array, $src, $image_meta, $attachment_id );

				if ( $srcset && ( $sizes || ! empty( $attr['sizes'] ) ) ) {
					$attr['srcset'] = $srcset;

					if ( empty( $attr['sizes'] ) ) {
						$attr['sizes'] = $sizes;
					}
				}
			}
		}

		/**
		 * Filters the list of attachment image attributes.
		 *
		 * @since 2.8.0
		 *
		 * @param array        $attr       Attributes for the image markup.
		 * @param WP_Post      $attachment Image attachment post.
		 * @param string|array $size       Requested size. Image size or array of width and height values
		 *                                 (in that order). Default 'thumbnail'.
		 */
		$attr = apply_filters( 'wp_get_attachment_image_attributes', $attr, $attachment, $size );
		$attr = array_map( 'esc_attr', $attr );
		$html = rtrim("<img $hwstring");
		foreach ( $attr as $name => $value ) {
			$html .= " $name=" . '"' . $value . '"';
		}
		$html .= ' />';
	}

	return $html;
}

function generic_getJsonData($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function fetchInstaData(){
	global $options;
	$insta_id=(!empty($options['socl']['insta_id'])) ? $options['socl']['insta_id'] : '5622210628';
	$insta_token=(!empty($options['socl']['insta_token'])) ? $options['socl']['insta_token'] : '5622210628.1677ed0.fbe08e99fae248fbbf009cfc45021390';
	$interval=(!empty($options['socl']['insta_interval'])) ? $options['socl']['insta_interval']*60 : '1800';
	$date=date('U');
	$opt=get_option( $options['prfx'].'insta_data', false );
	$prevdate=($opt!==false) ? maybe_unserialize( $opt )['date'] : $date;
	$data['date']=$date;
	if(empty($opt)||($date-$prevdate)>$interval){
		$url ='https://api.instagram.com/v1/users/'.$insta_id.'/media/recent/?access_token='.$insta_token;
		// $result=json_decode(generic_getJsonData($url));
		$result=generic_getJsonData($url);
		// if (!empty($result->pagination->next_url)) {
		// 	$check=$result->pagination->next_url;
		// 	$i=0;
		// 	do {
		// 		$check=($i!=0) ? json_decode(generic_getJsonData(((!empty($check)) ? $check : null)))->pagination->next_url : ((!empty($check)) ? $check : null);
		// 		$newdata=json_decode(generic_getJsonData($check))->data;
		// 		print_r($newdata);
		// 		$result->data=array_merge($result->data, $newdata);
		// 		$i++;
		// 	} while (!empty($check));
		// }
		$data['data']['date']=$date;
		$data['data']['rslt'][0]=$result;
		$upd['date']=$date;
		$upd['rslt'][0]=$result;
		update_option( $options['prfx'].'insta_data', maybe_serialize($upd), null );
		$data['get_opt']='0';
	} else {
		$data['data']=maybe_unserialize( $opt );
		$data['get_opt']='1';
	}

	$json=json_decode($data['data']['rslt'][0]);

	// print_r($data);
	echo '<div class="instagram_photo js-instagram_photo js-instagram-gallery">';
	$i=1;
	foreach ($json->data as $el) {
		// if($i<=20){
			$hidden=($i>8) ? ' hidden' : ' js-item';
			$ttl=(!empty($el->caption->text)) ? 'alt="'.$el->caption->text.'"' : null;
			$url=$el->images->standard_resolution->url;
			printf(
				'<a class="zoom-images%s" href="%s"><img class="img-responsive" src="%s" %s /></a>',
				$hidden,
				$url,
				$url,
				$ttl
			);
		// }
		$i++;
	}
	echo '</div>';

	if(!empty($json->pagination->next_url)){
		echo '<p class="center_p"><a class="btn btn-type-1 js-morePhotos" data-url="'.$json->pagination->next_url.'" href="#"><span class="btn-type-1__inner">'.__( 'more photos', 'giammetti' ).'</span></a></p>';
	}

}
function generic_ajax_get_photos(){
	global $options;
	$params=$_POST['params'];
	$url=(!empty($params['url'])) ? $params['url'] : null;
	if (empty($url)) {
		wp_send_json_error($params);
	}
	// $imgdata=maybe_unserialize(get_option( $options['prfx'].'insta_data', false ))->data;
	$imgdata=json_decode(generic_getJsonData($url));
	// $curdata=json_decode(get_option( $options['prfx'].'insta_data', false ));
	// $curdata['data']['rslt'][]=maybe_serialize($imgdata);
	// update_option( $options['prfx'].'insta_data', $curdata, null );
	// $json['imgdata']=$imgdata;
	ob_start();
	$i=1;
	foreach ($imgdata->data as $el) {
		$hidden=($i>4) ? ' hidden' : null;
		$ttl=(!empty($el->caption->text)) ? 'alt="'.$el->caption->text.'"' : null;
		$url=$el->images->standard_resolution->url;
		printf(
			'<a class="zoom-images js-zoom-images%s" href="%s"><img class="img-responsive" src="%s" %s /></a>',
			$hidden,
			$url,
			$url,
			$ttl
		);
		$i++;
	}
	$json['content']=ob_get_clean();
	$json['hide']=(empty($json->pagination->next_url)) ? true : false;
	wp_send_json_success($json);
}


// unwrap wpcf7 fields
// add_filter('wpcf7_form_elements', function($content) {
//	 $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
//	 return $content;
// });

// Remove each style one by one

function jk_dequeue_styles( $enqueue_styles ) {
	if (!is_cart()&&!is_checkout()) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
		unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	}

	return $enqueue_styles;
}
// remove woocommerce scripts on unnecessary pages
function woocommerce_de_script() {
	if (function_exists( 'is_woocommerce' )) {
		remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
		if (!is_cart() && !is_checkout() ) { // if we're not on a Woocommerce page, dequeue all of these scripts
			wp_dequeue_script('wc-add-to-cart');
			wp_dequeue_script('jquery-blockui');
			wp_dequeue_script('jquery-placeholder');
			wp_dequeue_script('woocommerce');
			wp_dequeue_script('jquery-cookie');
			wp_dequeue_script('wc-single-product');
			wp_dequeue_script('wc-cart-fragments');
			wp_deregister_script('wc-add-to-cart-variation');
		}
	}
}


function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
}

function shuffle_assoc($list) {
	if (!is_array($list)) return $list;

	$keys = array_keys($list);
	shuffle($keys);
	$random = array();
	foreach ($keys as $key) {
		$random[$key] = $list[$key];
	}
	return $random;
}


/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('cmb2_admin_init', 'generic_metaboxes' );
add_action('wp_enqueue_scripts', 'generic_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'generic_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'generic_styles'); // Add Theme Stylesheet
add_action('init', 'register_generic_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('init', 'generic_globals');
add_action('init', 'disable_wp_emojicons');
add_action('wp_footer', 'generic_footer_scripts');
add_action( 'wp_ajax_nopriv_generic_news_like', 'generic_news_like' );
add_action( 'wp_ajax_generic_news_like', 'generic_news_like' );
add_action( 'wp_ajax_nopriv_generic_ajax_posts', 'generic_ajax_posts' );
add_action( 'wp_ajax_generic_ajax_posts', 'generic_ajax_posts' );
add_action( 'wp_ajax_nopriv_generic_ajax_filterPosts', 'generic_ajax_filterPosts' );
add_action( 'wp_ajax_generic_ajax_filterPosts', 'generic_ajax_filterPosts' );
add_action('wp_ajax_nopriv_generic_ajax_cart', 'generic_ajax_cart' );
add_action('wp_ajax_generic_ajax_cart', 'generic_ajax_cart' );
add_action('wp_ajax_nopriv_generic_addToCart', 'generic_ajax_add_to_cart' );
add_action('wp_ajax_generic_addToCart', 'generic_ajax_add_to_cart' );
add_action('wp_ajax_nopriv_generic_ajax_get_photos', 'generic_ajax_get_photos' );
add_action('wp_ajax_generic_ajax_get_photos', 'generic_ajax_get_photos' );
add_action( 'pre_get_posts', 'posts_per_page_func' );

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 12);
add_action( 'wp_print_scripts', 'woocommerce_de_script', 999 );
add_action( 'wp_enqueue_scripts', 'woocommerce_de_script', 999 );



// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'generic_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
// add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'generic_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('post_thumbnail_html', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter('image_send_to_editor', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter( 'script_loader_src', 'remove_cssjs_ver', 15, 1 );
add_filter( 'style_loader_src', 'remove_cssjs_ver', 15, 1 );
// add_filter( 'wpcf7_load_css', '__return_false' );
add_filter( 'emoji_svg_url', '__return_false' );
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes above would be nested like this -
// [generic_shortcode_demo] [generic_shortcode_demo_2] Here's the page title! [/generic_shortcode_demo_2] [/generic_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5(){
	// register_taxonomy('collection', 'swimsuits',
	// 	array(
	// 	'labels' => array(
	// 		'name' => __('Collections', 'giammetti'), // Rename these to suit
	// 		'singular_name' => __('Collection', 'giammetti'),
	// 		'add_new' => __('Add New', 'giammetti'),
	// 		'add_new_item' => __('Add New Element', 'giammetti'),
	// 		'edit' => __('Edit', 'giammetti'),
	// 		'edit_item' => __('Edit element', 'giammetti'),
	// 		'new_item' => __('New element', 'giammetti'),
	// 		'view' => __('View elements', 'giammetti'),
	// 		'view_item' => __('View element', 'giammetti'),
	// 		'search_items' => __('Search element', 'giammetti'),
	// 		'not_found' => __('No elements found', 'giammetti'),
	// 		'not_found_in_trash' => __('No elements found in Trash', 'giammetti')
	// 	),
	// 	'public' => true,
	// 	// 'publicly_queryable' => false,
	// ));
	// register_taxonomy_for_object_type('collection', 'swimsuits');
	// register_taxonomy('style', 'swimsuits',
	// 	array(
	// 	'labels' => array(
	// 		'name' => __('Styles', 'giammetti'), // Rename these to suit
	// 		'singular_name' => __('Style', 'giammetti'),
	// 		'add_new' => __('Add New', 'giammetti'),
	// 		'add_new_item' => __('Add New Element', 'giammetti'),
	// 		'edit' => __('Edit', 'giammetti'),
	// 		'edit_item' => __('Edit element', 'giammetti'),
	// 		'new_item' => __('New element', 'giammetti'),
	// 		'view' => __('View elements', 'giammetti'),
	// 		'view_item' => __('View element', 'giammetti'),
	// 		'search_items' => __('Search element', 'giammetti'),
	// 		'not_found' => __('No elements found', 'giammetti'),
	// 		'not_found_in_trash' => __('No elements found in Trash', 'giammetti')
	// 	),
	// 	'public' => true,
	// 	'publicly_queryable' => false,
	// ));
	// register_taxonomy_for_object_type('style', 'swimsuits');
	// register_post_type('swimsuits', // Register Custom Post Type
	// 	array(
	// 	'labels' => array(
	// 		'name' => __('Swimsuits', 'giammetti'), // Rename these to suit
	// 		'singular_name' => __('Swimsuit', 'giammetti'),
	// 		'add_new' => __('Add New', 'giammetti'),
	// 		'add_new_item' => __('Add New Element', 'giammetti'),
	// 		'edit' => __('Edit', 'giammetti'),
	// 		'edit_item' => __('Edit element', 'giammetti'),
	// 		'new_item' => __('New element', 'giammetti'),
	// 		'view' => __('View elements', 'giammetti'),
	// 		'view_item' => __('View element', 'giammetti'),
	// 		'search_items' => __('Search element', 'giammetti'),
	// 		'not_found' => __('No elements found', 'giammetti'),
	// 		'not_found_in_trash' => __('No elements found in Trash', 'giammetti')
	// 	),
	// 	'public' => true,
	// 	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	// 	'has_archive' => true,
	// 	'supports' => array(
	// 		'title',
	// 		'editor',
	// 		'excerpt',
	// 		'thumbnail'
	// 	),
	// 	'can_export' => true
	// ));
}
