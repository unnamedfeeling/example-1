<?php get_header();
global $options;
$p=$options['prfx'];
$pmeta=get_post_meta( $post->ID, '', false );
$topimg=(!empty($pmeta[$p.'topimg_id'][0])) ? $pmeta[$p.'topimg_id'][0] : $options['tpld'].'/assets/img/bg_2.jpg';
?>
<div class="section-title-page area-bg area-bg_dark parallax" style="background-image: url(<?=$topimg?>)">
	<div class="area-bg__inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="<?=home_url()?>"><?=__( 'Home', 'giammetti' )?></a></li>
						<li class="active"><?=$post->post_title?></li>
					</ol>
					<h1 class="b-title-page"><?=$post->post_title?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="section-default">
	<div class="area-bg__inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
				<?php //comments_template( '', true ); // Remove if you don't want comments ?>
				<br class="clear">
				<?php edit_post_link(); ?>
			</article>
		<?php endwhile; ?>
		<?php else: ?>
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			</article>
		<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
