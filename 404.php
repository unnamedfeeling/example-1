<?php get_header();
global $options;
?>
<div class="section-title-page area-bg area-bg_dark parallax" style="background-image: url(<?=$options['tpld'].'/assets/img/bg_2.jpg'?>)">
	<div class="area-bg__inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="<?=home_url()?>"><?=__( 'Home', 'giammetti' )?></a></li>
						<li class="active">404</li>
					</ol>
					<h1 class="b-title-page"><?=__( '404 - That`s an error', 'giammetti' )?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="section-default">
	<div class="area-bg__inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<article id="post-404" <?php post_class(); ?>>
						<h1><?= __( 'Error 404. Page not found!', 'giammetti' ); ?></h1>
						<h2>
							<?php if(function_exists('icl_get_languages')){
								$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
							} else {
								$home_url='/';
							} ?>
							<a href="<?= $home_url ?>"><?php _e( 'Return home?', 'giammetti' ); ?></a>
						</h2>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
