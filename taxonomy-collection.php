<?php get_header();
global $options;
$obj=get_queried_object();
// print_r($obj);
$tid=$obj->term_id;
$tmeta=get_term_meta($tid, '', false);
$img=(!empty($tmeta[$options['prfx'].'collection_img_id'][0]))?wp_get_attachment_image_src( $tmeta[$options['prfx'].'collection_img_id'][0], 'full', false ) : $options['tpld'].'/assets/img/bg_3.jpg';
$all_swim_lnk=get_post_type_archive_link( 'swimsuits' );
if(function_exists('icl_get_languages')){
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
} else {
	$home_url='/';
}
?>
					<section class="section-title-page area-bg area-bg_dark  parallax" style="background-image: url(<?=$img?>)">
						<div class="area-bg__inner">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<ol class="breadcrumb">
											<li><a href="<?=$home_url?>"><?=__( 'Home', 'giammetti' )?></a></li>
											<li class="active"><?=$obj->name?></li>
										</ol>
										<h1 class="b-title-page"><?=$obj->name?></h1>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="collection">
						<div class="container_slo container">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-xs-12">
									<div class="b-works__inner">
										<h2 class="b-works__title">Lorem ipsum dolor sit amet</h2>
										<div class="b-works__content">Lorem ipsum dolor sit amet consectetur adipisic elit do eiusmod tempor enimad sandy minim sed ipsum ven quis nostrud exercitation.</div>
										<div class="clear"></div>
										<a class="btn btn-type-1" href="<?=$all_swim_lnk.'?collection='.$obj->term_id?>"><span class="btn-type-1__inner"><?=__( 'Shop collection ', 'giammetti' )?></span></a>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12">
									<img src="<?=$options['tpld']?>/assets/img/collection/1.jpg" alt="">
								</div>
							</div>
						</div>
						<div class="video">
							<div class="video-wrapper">
								<video poster="<?=$options['tpld']?>/assets/img/collection/poster.jpg" src="<?=$options['tpld']?>/assets/img/collection/video.mp4"></video>
							</div>
						</div>
					</section>
					<section class="l-main-content ">
						<div class="gallery_collection section-default b-isotope js-b-isotope">
							<div class="container">
								<h2 class="ui-title-block" id="revealfx2">Collection of swimsuits</h2>
								<div class="ui-subtitle-block">Photo gallery</span>
								</div>
							</div>
							<ul class="b-isotope-grid grid list-unstyled js-zoom-gallery">
								<li class="grid-sizer"></li>
								<li class="gutter-sizer"></li>
								<li class="b-isotope-grid__item grid-item design illustration">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/1.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 1</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/3.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 2</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item photography">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/6.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 3</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/8.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 4</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item photography">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/2.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 5</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/7.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 6</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item illustration ">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/4.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 7</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item grid-item_wx2 ">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/5.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 8</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item video design">
									<a class="b-isotope-grid__inner" href="#"><img src="<?=$options['tpld']?>/assets/img/monboard/9.jpg" alt="foto" /><span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical"><span class="b-isotope-grid__info"><span class="b-isotope-grid__title">Design 8</span><span class="b-isotope-grid__categorie">More</span></span>
								</span>
							</a>
								</li>
							</ul>
						</div>
					</section>
					<section class="section-blockquote-2">
						<div class="container">
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="owl-carousel owl-theme enable-owl-carousel" data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="7000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true">
										<!-- end .b-blockquote-->
										<div class="b-blockquote b-blockquote-1">
											<h2 class="b-works__title">Lorem ipsum dolor sit amet</h2>
											<blockquote>
												<p>I would greatly appreciate it if you would just, help me out here. Um, he's fine. No, absolutely not and I mean to keep it that way, so. Six-hundred and seventeen thousand dollars written out to me. Uh... what is this? Well, and why is that? It feels wrong. Uh huh. Well, um... Ted, the whole reason we're in this mess is because you had me cooking your books. So, when did wrong suddenly become a problem for you? </p>
											</blockquote>
										</div>
										<!-- end .b-blockquote-->
										<div class="b-blockquote b-blockquote-1">
											<h2 class="b-works__title">Lorem ipsum dolor sit amet</h2>
											<blockquote>
												<p>Ted, this is the big mistake right here. You owe the federal government six-hundred and seventeen thousand dollars. If you do not pay them, they will come after you, and then they will come after me. And if they audit my business, find out that Walt and I paid for it with close to a million dollars in untaxed gambling winnings. We will go to prison, where you will already be. Do you understand? Oh my god, how are you not following me here? </p>
											</blockquote>
										</div>
										<!-- end .b-blockquote-->
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="l-main-content moodboard">
						<div class="gallery_collection section-default b-isotope js-b-isotope">
							<div class="container">
								<h2 class="ui-title-block" id="revealfx2">Collection of swimsuits</h2>
								<div class="ui-subtitle-block">Moodboard</span>
								</div>
							</div>
							<ul class="b-isotope-grid grid list-unstyled js-zoom-gallery">
								<li class="grid-sizer"></li>
								<li class="gutter-sizer"></li>
								<li class="b-isotope-grid__item grid-item design illustration">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/1.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/1-1.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/2.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/3-3.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item photography">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/3.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/6-6.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/4.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/8-8.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item photography">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/5.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/2-2.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/6.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/7-7.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item illustration ">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/7.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/7.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item grid-item_wx2 ">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/8.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/5-5.jpg" alt="foto" />
							</a>
								</li>
								<li class="b-isotope-grid__item grid-item video design">
									<a class="b-isotope-grid__inner zoom-images js-zoom-images" href="assets/media/monboard/big/9.jpg"><img src="<?=$options['tpld']?>/assets/img/monboard/9.jpg" alt="foto" />
							</a>
								</li>
							</ul>
							<p class="center_p">
								<a class="btn btn-type-1" href="#"><span class="btn-type-1__inner">Load more photos</span></a>
							</p>
							<div></div>
						</div>
					</section>
<?php get_footer(); ?>
