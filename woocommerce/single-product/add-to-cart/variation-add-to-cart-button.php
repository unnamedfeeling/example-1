<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<input type="hidden" name="add-to-cart" value="<?= absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?= absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
	<input type="hidden" class="input-text qty text js-prodQty" name="quantity" value="1">
	<p class="pricedisplay js-priceDisplay"></p>
	<button type="submit" name="add-to-cart" value="<?= esc_attr( $product->get_id() ); ?>" data-product_id="<?= esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button"><?= esc_html( $product->single_add_to_cart_text() ); ?></button>
	<p class="js-error error" style="display:none"><?= __( 'You must select one of this product`s sizes to proceed further!', 'giammetti' ) ?></p>
</div>
