<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $options;

$attribute_keys = array_keys( $attributes );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart js-addToCart" method="post" enctype='multipart/form-data' data-product_id="<?= absint( $product->get_id() ); ?>" data-product_variations="<?= htmlspecialchars( wp_json_encode( $available_variations ) ) ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else :
		//print_r($attributes); ?>
		<table class="variations" cellspacing="0">
			<tbody>
				<?php foreach ( $attributes as $attribute_name => $opts ) : ?>
					<tr>
						<td class="label variation-label"><label for="<?= sanitize_title( $attribute_name ); ?>"><?= wc_attribute_label( $attribute_name ); ?></label></td>
						<td class="value">
							<ul class="varselector js-varselector">
								<?php
									// print_r($attribute_name);
									// print_r($options);
									foreach ($opts as $v) {
										printf('<li><input type="radio" value="%1$s" name="variation"id="%2$s" /><label for="%2$s">%1$s</label></li>',
										$v,
										'var-'.$v
										);
									}
									if ($attribute_name=='Size'||$attribute_name=='pa_size') { ?>
										<li class="size">
											<a href="#" class="mfp-image js-image-link" data-mfp-src="<?=(!empty($options['gnrl']['size_fit'])) ? $options['gnrl']['size_fit'] : '#'?>">
												<?=__( 'Size & Fit', 'giammetti' )?>
											</a>
										</li>
									<?php }
								?>
							</ul>
						</td>
						<td><?php do_action( 'woocommerce_before_add_to_cart_button' ); ?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="single_variation_wrap">
			<?php
				/**
				 * woocommerce_before_single_variation Hook.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * woocommerce_after_single_variation Hook.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>
</div>
<?php
do_action( 'woocommerce_after_add_to_cart_form' );
