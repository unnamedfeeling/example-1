<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
$large_image	   = wp_get_attachment_image_src( $post_thumbnail_id, 'product-fl' );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
	'view frame'
) );
?>
<div class="owl-carousel owl-theme big-image js-prodslider" data-slider-id="1">
	<?php
	$attributes = array(
		'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
		// 'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
		'data-src'                => $full_size_image[0],
		'data-large_image'        => $large_image[0],
		// 'data-large_image_width'  => $full_size_image[1],
		// 'data-large_image_height' => $full_size_image[2],
	);
	$controls='<div class="img-controls ready"><button class="zoom_out js-zoom_out" type="button" title="Zoom out"> <i class="fa fa-search-minus" aria-hidden="true"></i></button><button class="zoom_in js-zoom_in" type="button" title="Zoom in"><i class="fa fa-search-plus" aria-hidden="true"></i></button></div>';
	$thumbs='<div class="owl-carousel owl-theme owl-thumbs js-owl-thumbs" data-slider-id="1">';

	if ( has_post_thumbnail() ) {
		$attributes['data-index']=$post_thumbnail_id;
		// $html  = '<div class="sp-slide">';
		$html  = '<div class="slide js-item">';
		// $html .= wp_get_attachment_image( $post_thumbnail_id, 'product-lg', false, $attributes ).$controls;
		$html .= remove_width_attribute(wp_get_attachment_image( $post_thumbnail_id, 'product-lg', false, $attributes ));
		$thumbs .= '<div class="owl-thumb-item">'.remove_width_attribute(wp_get_attachment_image( $post_thumbnail_id, 'product-xs', false, array('class'=>'thumbnail', 'data-index'=>$post_thumbnail_id) )).'</div>';
		$html .= '</div>';
		// $html_thmb = get_the_post_thumbnail( $post->ID, 'thumbnail', $attributes );
	}

	$attachment_ids = $product->get_gallery_image_ids();

	if ( $attachment_ids && has_post_thumbnail() ) {
		foreach ( $attachment_ids as $attachment_id ) {
			$full_size_image = wp_get_attachment_image_src( $attachment_id, 'product-lg' );
			$large_image	 = wp_get_attachment_image_src( $attachment_id, 'product-fl' );
			// $thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
			$attributes      = array(
				'title'                   => get_post_field( 'post_title', $attachment_id ),
				'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
				// 'data-src'                => $full_size_image[0],
				'data-src'                => $large_image[0],
				'data-large_image'        => $full_size_image[0],
				// 'data-large_image_width'  => $full_size_image[1],
				// 'data-large_image_height' => $full_size_image[2],
				'data-index'			  => $attachment_id
			);
			// $html .= '<div class="sp-slide">';
			$html .= '<div class="slide js-item">';
			// $html .= wp_get_attachment_image_lazy( $attachment_id, 'shop_single', false, $attributes ).$controls;
			$html .= remove_width_attribute(wp_get_attachment_image_lazy( $attachment_id, 'product-lg', false, $attributes ));
			$thumbs .= '<div class="owl-thumb-item">'.remove_width_attribute(wp_get_attachment_image( $attachment_id, 'product-xs', false, array('class'=>'sp-thumbnail', 'data-index'=>$attachment_id) )).'</div>';
			$html .= '</div>';
			// $html_thmb .= wp_get_attachment_image( $attachment_id, 'thumbnail', false, $attributes );

			// echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
		}
	}

	$thumbs.='</div>';

	// echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );
	echo $html;

	?>
</div>
<?= $thumbs ?>
