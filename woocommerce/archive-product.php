<?php get_header();
global $options;
$obj=get_queried_object();
// print_r($obj);
if ($obj->name=='product') {
	$img=get_post_meta( get_option( 'woocommerce_shop_page_id' ), $options['prfx'].'topimg', true );
} else {
	$img=$options['tpld'].'/assets/img/bg_1.jpg';
}
if(function_exists('icl_get_languages')){
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
} else {
	$home_url='/';
}
?>
					<section class="section-title-page area-bg area-bg_dark  parallax" style="background-image: url(<?=$img?>)">
						<div class="area-bg__inner">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<ol class="breadcrumb">
											<li><a href="<?=$home_url?>"><?=__( 'Home', 'giammetti' )?></a></li>
											<li class="active"><?=$obj->name?></li>
										</ol>
										<h1 class="b-title-page"><?=$obj->name?></h1>
									</div>
								</div>
							</div>
						</div>
					</section>
					<div class="container">
						<div class="row">
							<?php get_sidebar(); ?>
							<div class="col-md-9 m_b">
								<ul id="swimsuitsContainer" class="b-isotope-grid b-isotope-grid_mod-a grid list-unstyled js-zoom-gallery js-swimsuitsCont" data-page="1">
									<li class="grid-sizer"></li>
									<li class="gutter-sizer"></li>
									<?php
									$args=array(
										'post_type'=>'product',
										'posts_per_page'=>12
									);
									$swim=new WP_Query($args);
									if ($swim->have_posts()): while ($swim->have_posts()) : $swim->the_post();
										get_template_part('loop', 'swim_grid-gen');
									endwhile;
									else : ?>
									<div class="noposts" style="width:100%;font-size:2rem;font-weight:bold;"><?=__( 'Nothing to display!', 'giammetti' )?></div>
									<?php endif; ?>
								</ul>
								<div class="clearfix"></div>
								<div class="text-center">
									<a id="archiveMoreSwimsuits" class="btn btn-type-1 btn-lg" href="#">
										<div>
											<span class="btn-type-1__inner"><?= __( 'load more items', 'giammetti' ); ?></span></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
<?php get_footer(); ?>
