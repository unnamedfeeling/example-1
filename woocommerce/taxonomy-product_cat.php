<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
global $options;
$obj=get_queried_object();
$p=$options['prfx'];
// print_r($obj);
$tid=$obj->term_id;
$tmeta=get_term_meta($tid, '', false);
$img=(!empty($tmeta[$options['prfx'].'collection_img_id'][0]))?wp_get_attachment_image_src( $tmeta[$options['prfx'].'collection_img_id'][0], 'full', false ) : $options['tpld'].'/assets/img/bg_3.jpg';
$all_swim_lnk=get_post_type_archive_link( 'product' );
if(function_exists('icl_get_languages')){
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
} else {
	$home_url='/';
}
?>
					<section class="section-title-page area-bg area-bg_dark  parallax" style="background-image: url(<?=$img[0]?>)">
						<div class="area-bg__inner">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<ol class="breadcrumb">
											<li><a href="<?=$home_url?>"><?=__( 'Home', 'giammetti' )?></a></li>
											<li class="active"><?=$obj->name?></li>
										</ol>
										<h1 class="b-title-page"><?=$obj->name?></h1>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="collection">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-xs-12">
									<div class="b-works__inner">
										<?php if (!empty($tmeta[$p.'collection_desc'][0])): ?>
											<?= apply_filters( 'the_content', $tmeta[$p.'collection_desc'][0] ) ?>
										<?php endif; ?>
										<div class="clear"></div>
										<a class="btn btn-type-1" href="<?=$all_swim_lnk.'?collection='.$obj->term_id?>"><span class="btn-type-1__inner"><?=__( 'Shop collection ', 'giammetti' )?></span></a>
									</div>
								</div>
								<?php if (!empty($tmeta[$p.'collection_desc_img_id'][0])): ?>
								<div class="col-lg-6 col-md-6 col-xs-12">
									<?= wp_get_attachment_image( $tmeta[$p.'collection_desc_img_id'][0], 'cat-catimg', false, array('alt'=>'Category: '.$obj->name) ) ?>
								</div>
								<?php endif; ?>

							</div>
						</div>
						<?php if (!empty($tmeta[$p.'ytvid'][0])): ?>
						<div class="video">
							<div class="video-wrapper">
								<div class="youtube-player" data-id="<?=$tmeta[$p.'ytvid'][0]?>"<?= (!empty($tmeta[$p.'ytvid_img_id'][0])) ? ' data-poster="'.wp_get_attachment_image_src( $tmeta[$p.'ytvid_img_id'][0], 'full', false )[0].'"' : null ?>></div>
								<svg class="video-overlay-play-button js-play" viewBox="0 0 200 200" alt="Play video"><circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"></circle><polygon points="70, 55 70, 145 145, 100" fill="#fff"></polygon></svg>
							</div>
						</div>
						<?php endif; ?>
					</section>

					<?php if (!empty($tmeta[$p.'slider'][0])):
						$slider=maybe_unserialize( $tmeta[$p.'slider'][0] ); ?>
					<section class="section-blockquote-2"<?=!empty($tmeta[$p.'slider_bg_id'][0])?' style="background-image:'.wp_get_attachment_image_src($tmeta[$p.'slider_bg_id'][0], 'full', false)[0].'"' : null?>>
						<div class="container">
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="owl-carousel owl-theme enable-owl-carousel" data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="7000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true">
										<?php foreach ($slider as $s): ?>
										<div class="b-blockquote b-blockquote-1">
											<h2 class="b-works__title"><?=$s['title']?></h2>
											<blockquote><?=$s['desc']?></blockquote>
										</div>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php endif; ?>
					<?php if (!empty($tmeta[$p.'collection_phgal_img'][0])):
						$moods=maybe_unserialize( $tmeta[$p.'collection_phgal_img'][0] ); ?>
						<section class="l-main-content moodboard">
							<div class="gallery_collection section-default b-isotope js-b-isotope">
								<div class="container">
									<h2 class="ui-title-block" id="revealfx2"><?=$obj->name?></h2>
									<div class="ui-subtitle-block"><?=__( 'Photo gallery', 'giammetti' )?></span>
									</div>
								</div>
								<ul class="b-isotope-grid list-unstyled js-light-gallery js-galPhotos" data-page="1">
									<li class="grid-sizer"></li>
									<li class="gutter-sizer"></li>
									<?php $i=0; foreach ($moods as $key => $value):
										$fullimg=wp_get_attachment_image_src( $key, 'full', false );
										$ratio=$fullimg[1]/$fullimg[2];
										if ($ratio>1.7) {
											$size='cat-galery-lg';
										} elseif ($ratio<1.2) {
											$size='cat-galery-md';
										} else {
											$size='cat-galery-sm';
										}
										$thumb=wp_get_attachment_image( $key, 'cat-moodboard', false, array('alt'=>$obj->name) );
										$opt=array('alt'=>$obj->name);
										$opt['class']=($i>3)?'hidden':null;
										$thumb=remove_width_attribute(wp_get_attachment_image( $key, $size, false, $opt ));
										?>
										<li class="b-isotope-grid__item<?=($i>3)?' hidden':' grid-item'?><?=($ratio>1.7)?' grid-item_wx2':null?>">
											<a class="b-isotope-grid__inner zoom-images<?=($i>3)?' hidden':' js-item'?>" href="<?=$fullimg[0]?>">
												<?=($i<=3)?$thumb:'<img src="'.$options['tpld'].'/assets/img/transparent.gif" data-fullimg="'.wp_get_attachment_image_src( $key, $size, false )[0].'" class="hidden" alt="'.$obj->name.'">'?>
											</a>
										</li>
									<?php $i++; endforeach; ?>
								</ul>
								<p class="center_p">
									<a class="btn btn-type-1 js-moreGalPhotos" href="#"><span class="btn-type-1__inner"><?=__( 'Load more photos', 'giammetti' )?></span></a>
								</p>
								<div></div>
							</div>
						</section>
					<?php endif; ?>
					<?php if (!empty($tmeta[$p.'collection_mood_img'][0])):
						$moods=maybe_unserialize( $tmeta[$p.'collection_mood_img'][0] ); ?>
						<section class="l-main-content moodboard">
							<div class="gallery_collection section-default b-isotope js-b-isotope">
								<div class="container">
									<h2 class="ui-title-block" id="revealfx2"><?=$obj->name?></h2>
									<div class="ui-subtitle-block"><?=__( 'Moodboard', 'giammetti' )?></span>
									</div>
								</div>
								<ul class="b-isotope-grid list-unstyled js-light-gallery js-moodPhotos" data-page="1">
									<li class="grid-sizer"></li>
									<li class="gutter-sizer"></li>
									<?php $i=0; foreach ($moods as $key => $value):
										$fullimg=wp_get_attachment_image_src( $key, 'full', false );
										$ratio=$fullimg[1]/$fullimg[2];
										if ($ratio>1.7) {
											$size='cat-galery-lg';
										} elseif ($ratio<1.2) {
											$size='cat-galery-md';
										} else {
											$size='cat-galery-sm';
										}
										$thumb=wp_get_attachment_image( $key, 'cat-moodboard', false, array('alt'=>$obj->name) );
										$opt=array('alt'=>$obj->name);
										$opt['class']=($i>3)?'hidden':null;
										$thumb=remove_width_attribute(wp_get_attachment_image( $key, $size, false, $opt ));
										?>
										<li class="b-isotope-grid__item<?=($i>3)?' hidden':' grid-item'?><?=($ratio>1.7)?' grid-item_wx2':null?>">
											<a class="b-isotope-grid__inner zoom-images<?=($i>3)?' hidden':' js-item'?>" href="<?=$fullimg[0]?>">
												<?=($i<=3)?$thumb:'<img src="'.$options['tpld'].'/assets/img/transparent.gif" data-fullimg="'.wp_get_attachment_image_src( $key, $size, false )[0].'" class="hidden" alt="'.$obj->name.'">'?>
											</a>
										</li>
									<?php $i++; endforeach; ?>
								</ul>
								<p class="center_p">
									<a class="btn btn-type-1 js-moreMoodPhotos" href="#"><span class="btn-type-1__inner"><?=__( 'Load more photos', 'giammetti' )?></span></a>
								</p>
								<div></div>
							</div>
						</section>
					<?php endif; ?>
					<?php if (have_posts()):
						$image_ids=array();
						$gallery_img_arr=array();
						$i=0;
						while (have_posts()) : the_post();
							$imgs=(!empty(get_post_meta( $post->ID, $p.'collection_mood_img', true )))?get_post_meta( $post->ID, $p.'collection_mood_img', true ) : null;
							if(count($imgs)>0){
								foreach ($imgs as $key => $value) {
									$thumb=wp_get_attachment_image_src( $key, 'product-lg', false )[0];
									ob_start();
									?>
							<li class="b-isotope-grid__item<?=($i>7)?' hidden':' grid-item'?> design illustration">
								<a class="b-isotope-grid__inner" href="<?=get_the_permalink( $post->ID )?>">
									<?php if ($i>7): ?>
										<img src="<?=$options['tpld']?>/assets/img/transparent.gif" data-fullimg="<?=$thumb?>" alt="<?=$post->post_title?>" class="hidden" />
									<?php else: ?>
										<img src="<?=$thumb?>" alt="<?=$post->post_title?>" />
									<?php endif; ?>
									<span class="b-isotope-grid__wrap-info hvr-shutter-in-vertical">
										<span class="b-isotope-grid__info">
											<span class="b-isotope-grid__title"><?=$post->post_title?></span>
											<span class="b-isotope-grid__categorie"><?=__( 'More', 'giammetti' )?></span>
										</span>
									</span>
								</a>
							</li>
									<?php
									$gallery_img_arr[]=ob_get_clean();
									$i++;
								}
							}
						endwhile;
						?>
						<section class="l-main-content ">
							<div class="gallery_collection section-default b-isotope js-b-isotope">
								<div class="container">
									<h2 class="ui-title-block" id="revealfx2"><?=$obj->name?></h2>
									<div class="ui-subtitle-block"><?=__( 'Swimsuits', 'giammetti' )?></span>
									</div>
								</div>
								<ul class="b-isotope-grid grid list-unstyled js-zoom-gallery">
									<li class="grid-sizer"></li>
									<li class="gutter-sizer"></li>
									<?php
									shuffle($gallery_img_arr);
									foreach ($gallery_img_arr as $key => $value) {
										echo $value;
									}
									?>
								</ul>
								<p class="center_p">
									<a class="btn btn-type-1 js-moreCatProducts" href="#"><span class="btn-type-1__inner"><?=__( 'Load more products', 'giammetti' )?></span></a>
								</p>
							</div>
						</section>
					<?php endif; ?>
<?php get_footer(); ?>
