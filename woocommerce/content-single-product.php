<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product, $options;
$p=$options['prfx'];
$pmeta=get_post_meta( $post->ID, '', false );
$topimg=(!empty($pmeta[$p.'topimg_id'][0])) ? $pmeta[$p.'topimg_id'][0] : $options['tpld'].'/assets/img/bg_2.jpg';
?>
<article id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="section-title-page area-bg area-bg_dark parallax" style="background-image: url(<?=wp_get_attachment_image_src($topimg, 'full')[0]?>)">
		<div class="area-bg__inner">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<ol class="breadcrumb">
							<li><a href="<?=home_url()?>"><?=__( 'Home', 'giammetti' )?></a></li>
							<li><a href="<?=get_post_type_archive_link('product')?>"><?=__( 'All swimsuits', 'giammetti' )?></a></li>
							<li class="active"><?=$post->post_title?></li>
						</ol>
						<h1 class="b-title-page"><?=$post->post_title?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container autumn">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-xs-12">
				<div class="gallery-box">
				<?php
					/**
					 * woocommerce_before_single_product_summary hook.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
				?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-xs-12 aut_item">
				<?php
					/**
					 * woocommerce_single_product_summary hook.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
				$size=(!empty($product->get_attribute( 'size' )))?$product->get_attribute( 'size' ):null;
				if(!$product->is_type('variable')&&!empty($size)){
				?>
				<div class="size"><?=__( 'Size:', 'giammetti' )?> <?=$size?>
					<a href="#" class="mfp-image js-image-link" data-mfp-src="<?=(!empty($options['gnrl']['size_fit'])) ? $options['gnrl']['size_fit'] : '#'?>">
						<?=__( 'Size & Fit', 'giammetti' )?>
					</a>
				</div>
				<?php
				}
				wc_display_product_attributes($product);
				the_content(); ?>
			</div>
		</div>
	</div>
</article>
