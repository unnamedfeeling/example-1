<?php get_header();
global $options;
$obj=get_queried_object();
// print_r($obj);
$img=$options['tpld'].'/assets/img/bg_1.jpg';
if(function_exists('icl_get_languages')){
	$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
} else {
	$home_url='/';
}
?>
					<section class="section-title-page area-bg area-bg_dark  parallax" style="background-image: url(<?=$img?>)">
						<div class="area-bg__inner">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<ol class="breadcrumb">
											<li><a href="<?=$home_url?>"><?=__( 'Home', 'giammetti' )?></a></li>
											<li class="active"><?=$obj->name?></li>
										</ol>
										<h1 class="b-title-page"><?=$obj->name?></h1>
									</div>
								</div>
							</div>
						</div>
					</section>
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="row">
									<div class="collection_wrap col-md-12 col-xs-6">
										<div class="ui-decor-1"></div>
										<h3 class="widget-title ui-title-inner">Collections</h3>
										<ul class="widget-list list-unstyled">
											<li class="widget-list__item"><a class="widget-list__link" href="#">Black& White</a></li>
											<li class="widget-list__item"><a class="widget-list__link" href="#">Corset</a></li>
										</ul>
									</div>
									<div class="collection_wrap col-md-12 col-xs-6">
										<div class="ui-decor-1"></div>
										<h3 class="widget-title ui-title-inner">Style</h3>
										<ul class="widget-list list-unstyled">
											<li class="widget-list__item"><a class="widget-list__link" href="#">Set</a></li>
											<li class="widget-list__item"><a class="widget-list__link" href="#">Top</a></li>
											<li class="widget-list__item"><a class="widget-list__link" href="#">Bottom</a></li>
											<li class="widget-list__item"><a class="widget-list__link" href="#">One piece</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-9 m_b">
								<ul class="b-isotope-grid b-isotope-grid_mod-a grid list-unstyled js-zoom-gallery">
									<li class="grid-sizer"></li>
									<li class="gutter-sizer"></li>
									<?php
									$args=array(
										'post_type'=>'swimsuits',
										'posts_per_page'=>12
									);
									$swim=new WP_Query($args);
									if ($swim->have_posts()): while ($swim->have_posts()) : $swim->the_post();
										get_template_part('loop', 'swim_grid-gen');
									endwhile;
									else : ?>
									<article>
										<h1><?= __( 'Sorry, nothing to display.', 'giammetti' ); ?></h1>
									</article>
									<?php endif; ?>
								</ul>
								<div class="clearfix"></div>
								<div class="text-center">
									<a id="archiveMoreSwimsuits" class="btn btn-type-1 btn-lg" href="#">
										<div>
											<span class="btn-type-1__inner"><?= __( 'load more items', 'giammetti' ); ?></span></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
<?php get_footer(); ?>
