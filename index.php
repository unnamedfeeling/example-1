<?php get_header(); ?>
<section class="section-news section-default">
	<div class="area-bg__inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
			<h1><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
