<?php
function generic_metaboxes() {
	// Start with an underscore to hide fields from custom fields list
	global $options;
	// $p = 'startime_';
	$p = $options['prfx'];

	/**
	 * Initiate the metabox
	 */
	// if(isset($_GET['post'])){
	// 	$post_id = $_GET['post'];
	// } elseif(isset($_POST['post_ID'])){
	// 	$post_id = $_POST['post_ID'];
	// } else {
	// 	$post_id='';
	// }
	// $mpost=get_post($post_id);
	// print_r($post_id);
	//$post_id = get_the_ID() ;
	// $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// $pt=get_post_type($post_id);
	// $pt=get_current_screen()->post_type;
	// check for a template type
	$cmb_main = new_cmb2_box( array(
		'id'            => 'main_metabox',
		'title'         => __( 'This page data', 'giammetti' ),
		'object_types'  => array( 'page', ), // Post type
		'show_on'       => array( 'key' => 'page-template', 'value' => 'template-main.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$main_slider = $cmb_main->add_field( array(
		'id'          => $p.'mainslider',
		'type'        => 'group',
		'description' => __( 'Main slider content', 'giammetti' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => __( 'Slide {#}', 'giammetti' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => __( 'Add', 'giammetti' ),
			'remove_button' => __( 'Delete', 'giammetti' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );
	$cmb_main->add_group_field( $main_slider, array(
		'name' => __( 'Image', 'giammetti' ),
		'id'   => 'image',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_main->add_group_field( $main_slider, array(
		'name' => __( 'Slide content', 'giammetti' ),
		'id'   => 'descr',
		'type' => 'wysiwyg'
	) );
	$cmb_main->add_field( array(
		'name' => __( 'About us image', 'giammetti' ),
		'id'   => $p.'about_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_main->add_field( array(
		'name'    => __( 'Attached products', 'giammetti' ),
		// 'desc'    => __( 'Select which programs to show on this page', 'giammetti' ),
		'id'      => $p.'attached_swim',
		'type'    => 'custom_attached_posts',
		'options' => array(
			'show_thumbnails' => false, // Show thumbnails on the left
			'filter_boxes'    => false, // Show a text box for filtering the results
			'query_args'      => array(
				'posts_per_page' => 999,
				'post_type'      => 'product',
			)
		),
	) );
	$main_about = $cmb_main->add_field( array(
		'id'          => $p.'aboutus',
		'type'        => 'group',
		'description' => __( 'Content for "About us" block', 'giammetti' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => __( 'Element {#}', 'giammetti' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => __( 'Add', 'giammetti' ),
			'remove_button' => __( 'Delete', 'giammetti' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Title', 'giammetti' ),
		'id'   => 'title',
		'type' => 'text',
		'sanitization_cb' => 'unfilter_all'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Title link', 'giammetti' ),
		'id'   => 'title_lnk',
		'type' => 'text'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Content', 'giammetti' ),
		'id'   => 'content',
		'type' => 'wysiwyg'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 1 image', 'giammetti' ),
		'id'   => 'img_evt1',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 1 title', 'giammetti' ),
		'id'   => 'title_evt1',
		'type' => 'text'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 1 link', 'giammetti' ),
		'id'   => 'lnk_evt1',
		'type' => 'text'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 2 image', 'giammetti' ),
		'id'   => 'img_evt2',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 2 title', 'giammetti' ),
		'id'   => 'title_evt2',
		'type' => 'text'
	) );
	$cmb_main->add_group_field( $main_about, array(
		'name' => __( 'Event 2 link', 'giammetti' ),
		'id'   => 'lnk_evt2',
		'type' => 'text'
	) );
	$cmb_main->add_field( array(
		'name' => __( 'Collections description text', 'giammetti' ),
		'id'   => $p.'collections_descr',
		'type' => 'wysiwyg'
	) );
	$cmb_main->add_field( array(
		'name' => __( 'Contact form', 'giammetti' ),
		'id'   => $p.'contact_sc',
		'type' => 'text'
	) );
	$cmb_main->add_field( array(
		'name' => __( 'Contact form image', 'giammetti' ),
		'id'   => $p.'contact_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );

	// swimsuits metabox
	$cmb_swim = new_cmb2_box( array(
		'id'            => 'swim_metabox',
		'title'         => __( 'This page data', 'giammetti' ),
		'object_types'  => array( 'swimsuits', 'product' ), // Post type
		// 'show_on'       => array( 'key' => 'page-template', 'value' => 'template-main.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	// $cmb_swim->add_field( array(
	// 	'name' => __( 'Price', 'giammetti' ),
	// 	'id'   => $p.'price',
	// 	'type' => 'text',
	// 	'attributes' => array(
	// 		'type' => 'number',
	// 		'pattern' => '\d*',
	// 		'step' => '1',
	// 		'min' => '1',
	// 	),
	// 	'sanitization_cb' => 'int',
	// 	'escape_cb'       => 'int',
	// ) );
	$cmb_swim->add_field( array(
		'name' => __( 'Header image', 'giammetti' ),
		// 'desc' => __( 'for a custom designs on an archive page', 'giammetti' ),
		'id'   => $p.'topimg',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_swim->add_field( array(
		'name' => __( 'Design photos', 'giammetti' ),
		'desc' => __( 'for a custom designs on an archive page', 'giammetti' ),
		'id'   => $p.'collection_mood_img',
		'type' => 'file_list',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );

	// page metabox
	$cmb_page = new_cmb2_box( array(
		'id'            => 'page_metabox',
		'title'         => __( 'This page data', 'giammetti' ),
		'object_types'  => array( 'page' ), // Post type
		// 'show_on'       => array( 'key' => 'page-template', 'value' => 'template-main.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_page->add_field( array(
		'name' => __( 'Header image', 'giammetti' ),
		// 'desc' => __( 'for a custom designs on an archive page', 'giammetti' ),
		'id'   => $p.'topimg',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );

	// category term metabox
	$cmb_coll = new_cmb2_box( array(
		'id'            => 'collection_metabox',
		'title'         => __( 'This category data', 'giammetti' ),
		'object_types'  => array( 'term', ), // Post type
		'taxonomies'       => array( 'product_cat' ),
		// 'show_on'       => array( 'key' => 'page-template', 'value' => 'template-main.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Custom product category meta', 'giammetti' ),
		'id'   => $p.'title',
		'type' => 'title'
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Category image (Home)', 'giammetti' ),
		'id'   => $p.'main-collection_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Category image (Category page)', 'giammetti' ),
		'id'   => $p.'collection_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Category description', 'giammetti' ),
		'id'   => $p.'collection_desc',
		'type' => 'wysiwyg',
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Category description image', 'giammetti' ),
		'id'   => $p.'collection_desc_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Youtube video ID', 'giammetti' ),
		'id'   => $p.'ytvid',
		'type' => 'text'
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Youtube video poster', 'giammetti' ),
		'id'   => $p.'ytvid_img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Slider background', 'giammetti' ),
		'id'   => $p.'slider_bg',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$coll_slider = $cmb_coll->add_field( array(
		'id'          => $p.'slider',
		'type'        => 'group',
		'description' => __( 'Content for text slider', 'giammetti' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => __( 'Element {#}', 'giammetti' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => __( 'Add', 'giammetti' ),
			'remove_button' => __( 'Delete', 'giammetti' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );
	$cmb_coll->add_group_field( $coll_slider, array(
		'name' => __( 'Title', 'giammetti' ),
		'id'   => 'title',
		'type' => 'text',
		'sanitization_cb' => 'unfilter_all'
	) );
	$cmb_coll->add_group_field( $coll_slider, array(
		'name' => __( 'Description', 'giammetti' ),
		'id'   => 'desc',
		'type' => 'wysiwyg',
		// 'sanitization_cb' => 'unfilter_all'
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Images for Photo gallery', 'giammetti' ),
		'id'   => $p.'collection_phgal_img',
		'type' => 'file_list',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );
	$cmb_coll->add_field( array(
		'name' => __( 'Images for Moodboard', 'giammetti' ),
		'id'   => $p.'collection_mood_img',
		'type' => 'file_list',
		'query_args' => array(
			'type' => array('image/jpeg')
		)
	) );

}
