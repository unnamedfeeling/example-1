(function(root, $, undefined) {
	"use strict";

	updateCart();

	if ($('.js-swimsuitsCont').length) {
		// var $swgrid=$('.js-swimsuitsCont').isotope({
		// 	itemSelector: '.grid-item',
		// 	// percentPosition: true,
		// 	masonry: {
		// 		// columnWidth: '.grid-sizer',
		// 		gutter: '.gutter-sizer'
		// 	}
		// });
		var $swgrid;
		$swgrid=$('.js-swimsuitsCont').imagesLoaded( function() {
			$swgrid.isotope({
				itemSelector: '.grid-item',
				// percentPosition: true,
				masonry: {
					columnWidth: '.grid-item',
					gutter: '.gutter-sizer'
				}
			})
		});
	}

	/*
	| ----------------------------------------------------------------------------------
	| TABLE OF CONTENT
	| ----------------------------------------------------------------------------------
	-SETTING
	-Preloader
	-Scroll Animation
	-Parallax(Stellar)
	-Animated Entrances
	-Chars Start
	-Loader blocks
	-Accordion
	-Tooltip
	-Zoom Images
	-Isotope filter
	-Select customization
	-Main slider
	-OWL Sliders
	-Image animation
	*/

	// function screenerGo() {
	// 	$('.screen-loader').removeClass("screen-end");
	// 	$('.screen-loader').addClass("screen-start");
	// }
 	var ajaxPost=function(target, type, container, func, quant, opts){
		// console.log(target);
		var q=(typeof quant!=='undefined') ? quant : 4,
			cont=document.getElementById(container),
			page = parseInt(cont.dataset.page),
			// total = parseInt(swimsuitsContainer.dataset.total),
			cat = (!isNaN(cont.dataset.cat))?parseInt(cont.dataset.cat):'',
			catid = (!isNaN($(target).get(0).dataset.cat))?parseInt($(target).get(0).dataset.cat):'',
			tagid = (!isNaN($(target).get(0).dataset.tagid))?parseInt($(target).get(0).dataset.tagid):'',
			tagslug = ($(target).get(0).dataset.tagslug!=='')?$(target).get(0).dataset.tagslug:'',
			// data={p: page, t: total, c: cat, pt: 'product'},
			data={p: page, c: ((cat!=='') ? cat : catid), pt: type, q: q, t: tagid},
			postcount=parseInt($('#'+container).find('.grid-item').length),
			excl=(typeof opts!=='undefined' && typeof opts.excl!=='undefined')?opts.excl : null;
		if (data.c=='') {
			delete data.c;
		}
		if (typeof excl !== 'undefined'&&excl !== ''&&excl !== null) {
			data.e=excl;
		}
		// console.log('load');
		// console.log(data);
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: func,
				params: data
			},
			beforeSend: function(){
				if(!$(target).hasClass('js-filterCat')&&!$(target).hasClass('js-filterTag')){
					$(target).attr('disabled', 'disabled').css('opacity', '0.7');
				}

			},
			success: function (data) {
				if($('#'+container).find('.noposts').length){
					$('#'+container).find('.noposts').remove();
				}
				$('#archiveMoreSwimsuits').fadeIn(500)
				// data.content=(data.content.search('noposts'))?'<li class="b-isotope-grid__item grid-item design fullwidth">'+data.content+'</li>':data.content;
				var h=$swgrid.innerHeight(),
					content=$.parseHTML(data.content),
					c;
				if ($(content).find('.noposts').length) {
					// $(content).wrap('<li class="b-isotope-grid__item grid-item design fullwidth"></li>');
					// console.log('noposts');
					$('#archiveMoreSwimsuits').fadeOut(300);
				}
				// console.log(data);
				// console.log(data.content);
				$('#'+container).css('opacity', '0.1');
				// var content=$.parseHTML(data.content.replace('/data-src/g', 'src'));
				postcount+=$(content).find('.grid-item').length;
				// $swgrid.append($(content));
				$swgrid.append($(content)).isotope('appended', $(content)).isotope('layout');
				setTimeout(function(){
					$swgrid.append($(content)).imagesLoaded(function(){
						// $swgrid.isotope('appended', $(content) ).isotope('layout');
						// console.log(new Date());
						$swgrid.isotope('layout');
					});
				}, 300);

				// $swgrid.isotope('layout');
				// $swgrid.append($(content)).isotope('appended', $(content)).isotope('layout');
				// if((parseInt($swgrid.innerHeight())-parseInt(h))<300){
				// 	setTimeout(function(){
				// 		$swgrid.isotope('layout');
				// 	}, 300)
				// }
				// $swgrid.append(content);
				// $swgrid.append(data.content).imagesLoaded().progress( function() {
				// 	$swgrid.isotope('appended', data.content);
				// })
				$('#'+container).css('opacity', '1');
				// swimsuitsContainer.dataset.page++;
				$('#'+container).get(0).dataset.page++;
				if (typeof data.pids!=='undefined') {
					// console.log(data.pids);
					// console.log($('#'+container).data('excl'));
					// console.log(container);
					var ex_ids=String(data.pids)+String($('#'+container).get(0).dataset.excl);
					// console.log(ex_ids);
					$('#'+container).removeAttr('data-excl');
					// $('#'+container).data('excl', ex_ids);
					$('#'+container).attr('data-excl', ex_ids);
				}
				var totalposts=parseInt(data.total_posts);
				// console.log('totalposts:'+totalposts);
				// console.log('postcount:'+postcount);
				// console.log(parseInt(postcount)>(parseInt(totalposts)-1));
				if(!$(target).hasClass('js-filterCat')&&!$(target).hasClass('js-filterTag')){
					if(parseInt(postcount)>(parseInt(totalposts)-1)){
						$(target).addClass('hidden').fadeOut(300);
					} else {
						$(target).removeAttr('disabled').css('opacity', '1');
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
	};
	$(document).on('click', '#mainMoreSwimsuits', function(event){
		// console.log($('#swimsuitsContainer').data('excl'));
		// var opts={excl:$('#swimsuitsContainer').data('excl')},
		var opts={excl:swimsuitsContainer.dataset.excl},
			quant=JSON.parse("[" + opts.excl + "]").length;
		// console.log(opts);
		ajaxPost('#mainMoreSwimsuits', 'product', 'swimsuitsContainer', 'generic_ajax_posts', quant, opts);
		event.preventDefault();
		// opts='';
	})
	$(document).on('click', '#archiveMoreSwimsuits', function(event){
		ajaxPost('#archiveMoreSwimsuits', 'product', 'swimsuitsContainer', 'generic_ajax_posts', 12);
		event.preventDefault();
	})
	$(document).on('click', '.js-filterCat', function(event){
		swimsuitsContainer.dataset.cat=event.target.dataset.catid;
		// $swgrid.isotope('remove', $('#swimsuitsContainer').find('.b-isotope-grid__item')).isotope('layout');
		$swgrid.isotope('remove', $('#swimsuitsContainer').find('.b-isotope-grid__item')).isotope('layout');
		ajaxPost(event.target, 'product', 'swimsuitsContainer', 'generic_ajax_filterPosts', 12);
		swimsuitsContainer.dataset.page=0;
		if($('#swimsuitsContainer').parent().find('.hidden').length){
			$('#swimsuitsContainer').parent().find('.hidden').show(300).removeClass('hidden');
		}
		// console.log(event.target);
		event.preventDefault();
	})
	$(document).on('click', '.js-filterTag', function(event){
		swimsuitsContainer.dataset.tagid=event.target.dataset.tagid;
		// $swgrid.isotope('remove', $('#swimsuitsContainer').find('.b-isotope-grid__item')).isotope('layout');
		$swgrid.isotope('remove', $('#swimsuitsContainer').find('.b-isotope-grid__item')).isotope();
		ajaxPost(event.target, 'product', 'swimsuitsContainer', 'generic_ajax_filterPosts', 12);
		swimsuitsContainer.dataset.page=0;
		if($('#swimsuitsContainer').parent().find('.hidden').length){
			$('#swimsuitsContainer').parent().find('.hidden').show(300).removeClass('hidden');
		}
		// console.log(event.target);
		event.preventDefault();
	})

	$(document).ready(function() {

		// "use strict";

		// if ($('.js-error').length) {
        //
		// }



		/////////////////////////////////////////////////////////////////
		// LOADER
		/////////////////////////////////////////////////////////////////



		$('.yamm  a , .b-title-page__list li a , .breadcrumb a').on("click", function(event) {
			event.preventDefault();
			if (!$(this).hasClass('dropdown-toggle')) {
				// $('.screen-loader').removeClass("screen-start");
				// $('.screen-loader').addClass("screen-end");
				// $('.loading').removeClass("loading-hide");

				// setTimeout(screenerGo, 2000);

				var goTo = this.getAttribute("href");
				setTimeout(function() {
					window.location = goTo;
				}, 700);
				setTimeout(loadingHide, 1400);
			} else {
				$(this).removeClass('dropdown-toggle')
			}
		});

		// var $preloader = $('#page-preloader'),
		// 	$spinner = $preloader.find('.spinner-loader');
		// $spinner.fadeOut();
		// $preloader.delay(50).fadeOut('slow');
		// setTimeout(screenerGo, 1500);
		// setTimeout(loadingHide, 2300);

		/////////////////////////////////////
		//  Scroll Animation
		/////////////////////////////////////

		if ($('.scrollreveal').length > 0) {
			window.sr = ScrollReveal({
				reset: true,
				duration: 1000,
				delay: 200
			});

			sr.reveal('.scrollreveal');
		}




		/**
		 * Portfolio sticky menu
		 */




		$(".sticky-bar").stick_in_parent();






		//////////////////////////////
		// Parallax(Stellar)
		//////////////////////////////

		if ($('.stellar').length > 0) {
			$.stellar({
				responsive: true
			});
		}


		/////////////////////////////////////
		//  Chars Start
		/////////////////////////////////////


		if ($('body').length) {
			$(window).on('scroll', function() {
				var winH = $(window).scrollTop();

				$('.b-progress-list').waypoint(function() {
					$('.js-chart').each(function() {
						CharsStart();
					});
				}, {
					offset: '80%'
				});
			});
		}


		function CharsStart() {

			$('.js-chart').easyPieChart({
				barColor: false,
				trackColor: false,
				scaleColor: false,
				scaleLength: false,
				lineCap: false,
				lineWidth: false,
				size: false,
				animate: 5000,

				onStep: function(from, to, percent) {
					$(this.el).find('.js-percent').text(Math.round(percent));
				}
			});
		}




		/////////////////////////////////////
		//  Loader blocks
		/////////////////////////////////////


		$(".js-scroll-next").on("click", function() {

			var hiddenContent = $(".js-scroll-next + .js-scroll-content");

			$(".js-scroll-next").hide();
			hiddenContent.show();
			hiddenContent.addClass("animated");
			hiddenContent.addClass("animation-done");
			hiddenContent.addClass("bounceInUp");

		});



		/////////////////////////////////////////////////////////////////
		// Accordion
		/////////////////////////////////////////////////////////////////

		$(".btn-collapse").on('click', function() {
			$(this).parents('.panel-group').children('.panel').removeClass('panel-default');
			$(this).parents('.panel').addClass('panel-default');
			if ($(this).is(".collapsed")) {
				$('.panel-title').removeClass('panel-passive');
			} else {
				$(this).next().toggleClass('panel-passive');
			};
		});




		/////////////////////////////////////
		//  Tooltip
		/////////////////////////////////////


		// $('.link-tooltip-1').tooltip({
		// 	template: '<div class="tooltip tooltip-1" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
		// });
		// $('.link-tooltip-2').tooltip({
		// 	template: '<div class="tooltip tooltip-2" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
		// });





		/////////////////////////////////////
		//  Zoom Images
		/////////////////////////////////////



		if ($('.js-zoom-gallery').length > 0) {
			$('.js-zoom-gallery').each(function() { // the containers for all your galleries
				$(this).magnificPopup({
					delegate: '.js-zoom-gallery__item', // the selector for gallery item
					type: 'image',
					gallery: {
						enabled: true
					},
					mainClass: 'mfp-with-zoom', // this class is for CSS animation below

					zoom: {
						enabled: true, // By default it's false, so don't forget to enable it

						duration: 300, // duration of the effect, in milliseconds
						easing: 'ease-in-out', // CSS transition easing function

						// The "opener" function should return the element from which popup will be zoomed in
						// and to which popup will be scaled down
						// By defailt it looks for an image tag:
						opener: function(openerElement) {
							// openerElement is the element on which popup was initialized, in this case its <a> tag
							// you don't need to add "opener" option if this code matches your needs, it's defailt one.
							return openerElement.is('img') ? openerElement : openerElement.find('img');
						}
					}
				});
			});
		}
		if ($('.js-light-gallery').length > 0) {
			$('.js-light-gallery').each(function() { // the containers for all your galleries
				$(this).lightGallery({
					selector: '.js-item',
					thumbnail:false,
					zoom: true,
					autoplay: false,
					hash: false,
					share: false
				});
			});
		}
		if ($('.js-instagram-gallery').length > 0) {
			$('.js-instagram-gallery').each(function() { // the containers for all your galleries
				$(this).lightGallery({
					selector: '.js-item',
					thumbnail:false,
					zoom: true,
					autoplay: false,
					hash: false,
					share: false
				});
			});
		}

		if ($('.js-zoom-images').length > 0) {
			zoomImg();
		}

		// if ($('#instagram').length) {
		// 	var container=$('#instagram').find('.instagram_photo');
		// 	var feed = new Instafeed({
		// 		get: 'user',
		// 		target: 'instagram_photo',
		// 		userId: '4310952700',
		// 		clientId: '77e5ed9dc877476297b3e1246153f1ca',
		// 		filter: function(image) {
		// 			return image.tags.indexOf('TAG_NAME') >= 0;
		// 		},
		// 		template: '<a class="zoom-images zoom-images js-zoom-images" href="{{image}}"><img class="img-responsive" src="{{image}}" alt="Foto" /></a>'
		// 	});
		// 	feed.run();
		// }



		if ($('.popup-youtube').length > 0) {
			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,

				fixedContentPos: false
			});
		}


		////////////////////////////////////////////
		// ISOTOPE FILTER
		///////////////////////////////////////////


		// if ($('.bslide-isotope').length > 0) {
		//
		// 	var $container = $('.bslide-isotope-grid');
		//
		// 	// init Isotope
		// 	var $grid = $('.grid').isotope({
		// 		itemSelector: '.grid-item',
		// 		// percentPosition: true,
		// 		masonry: {
		// 			columnWidth: '.grid-sizer',
		// 			gutter: '.gutter-sizer'
		// 		}
		// 	});
		// 	// layout Isotope after each image loads
		// 	$grid.imagesLoaded().progress(function() {
		// 		$grid.isotope('layout');
		// 	});
		//
		// 	// filter items when filter link is clicked
		// 	// $('.b-isotope-filter a').on('click', function() {
		// 	// 	var selector = $(this).attr('data-filter');
		// 	// 	$container.isotope({
		// 	// 		filter: selector
		// 	// 	});
		// 	// 	return false;
		// 	// });
		// 	//
		// 	// $('.b-isotope-filter a').on('click', function() {
		// 	// 	$('.b-isotope-filter').find('.current').removeClass('current');
		// 	// 	$(this).parent().addClass('current');
		// 	// });
		// }

		// var $container, $grid;
		var $grid, $moodGrid, $galGrid;

		if ($('.js-b-isotope').length > 0) {

			// $container = $('.b-isotope-grid');

			// init Isotope
			$grid = $('.js-b-isotope .grid');
			$grid.isotope({
				itemSelector: '.grid-item',
				percentPosition: true,
				masonry: {
					columnWidth: '.grid-sizer',
					gutter: '.gutter-sizer'
				}
			});
			// layout Isotope after each image loads
			$grid.imagesLoaded().progress(function() {
				$grid.isotope('layout');
			});
			$moodGrid = $('.js-moodPhotos');
			// console.log($moodGrid);
			$moodGrid.isotope({
				itemSelector: '.grid-item',
				// percentPosition: true,
				masonry: {
					columnWidth: '.grid-sizer',
					gutter: '.gutter-sizer'
				}
			});
			// layout Isotope after each image loads
			$moodGrid.imagesLoaded().progress(function() {
				$moodGrid.isotope('layout');
			});
			$galGrid = $('.js-galPhotos');
			$galGrid.isotope({
				itemSelector: '.grid-item',
				// percentPosition: true,
				masonry: {
					columnWidth: '.grid-sizer',
					gutter: '.gutter-sizer'
				}
			});
			// layout Isotope after each image loads
			$galGrid.imagesLoaded().progress(function() {
				$galGrid.isotope('layout');
			});


			// filter items when filter link is clicked
			// $('.b-isotope-filter a').on('click', function() {
			// 	var selector = $(this).attr('data-filter');
			// 	$container.isotope({
			// 		filter: selector
			// 	});
			// 	return false;
			// });
			//
			// $('.b-isotope-filter a').on('click', function() {
			// 	$('.b-isotope-filter').find('.current').removeClass('current');
			// 	$(this).parent().addClass('current');
			// });
		}
		// load more photos for moodboard
		$(document).on('click', '.js-moreMoodPhotos', function(event){
			event.preventDefault();
			// console.log($moodGrid);
			var btn=$(this);
			if($moodGrid.find('li.hidden').length){
				var hid=$moodGrid.find('li.hidden').slice(0, 4);
				hid.each(function() {
					var th=$(this), img=th.find('img.hidden');
					// console.log(img.data('fullimg'));
					img.attr('src', img.data('fullimg'))
					th.toggleClass('hidden grid-item');
					th.find('.b-isotope-grid__inner').toggleClass('hidden js-item');
					img.toggleClass('hidden').removeAttr('style');
					img.imagesLoaded().progress(function() {
						$moodGrid.isotope('appended', th);
					});
				});
				if (!$moodGrid.find('li.hidden').length) {
					btn.fadeOut(300);
				}
			}
			btn.closest('.js-b-isotope').find('.js-light-gallery').data('lightGallery').destroy(true);
			btn.closest('.js-b-isotope').find('.js-light-gallery').lightGallery({
				selector: '.js-item',
				thumbnail:false,
				zoom: true,
				autoplay: false,
				hash: false,
				share: false
			});
			// console.log($moodGrid);
		});
		// load more photos for photo gallery
		$(document).on('click', '.js-moreGalPhotos', function(event){
			event.preventDefault();
			var btn=$(this);
			if($galGrid.find('li.hidden').length){
				var hid=$galGrid.find('li.hidden').slice(0, 4);
				hid.each(function() {
					var th=$(this), img=th.find('img.hidden');
					// console.log(img.data('fullimg'));
					img.attr('src', img.data('fullimg'))
					th.toggleClass('hidden grid-item');
					th.find('.b-isotope-grid__inner').toggleClass('hidden js-item');
					img.toggleClass('hidden').removeAttr('style');
					img.imagesLoaded().progress(function() {
						$galGrid.isotope('appended', th);
					});
				});
				if (!$galGrid.find('li.hidden').length) {
					btn.fadeOut(300);
				}
			}
			btn.closest('.js-b-isotope').find('.js-light-gallery').data('lightGallery').destroy(true);
			btn.closest('.js-b-isotope').find('.js-light-gallery').lightGallery({
				selector: '.js-item',
				thumbnail:false,
				zoom: true,
				autoplay: false,
				hash: false,
				share: false
			});
			// console.log($moodGrid);
		});
        // show more hidden products
		$(document).on('click', '.js-moreCatProducts', function(event){
			console.log($grid);
			event.preventDefault();
			var btn=$(this),
				prodsCont=btn.closest('.js-b-isotope').find('.grid');
			if(prodsCont.find('li.hidden').length){
				var hid=prodsCont.find('li.hidden').slice(0, 4);
				hid.each(function() {
					var th=$(this), img=th.find('img.hidden');
					console.log(img.data('fullimg'));
					img.attr('src', img.data('fullimg'))
					th.toggleClass('hidden grid-item');
					// th.find('.b-isotope-grid__inner').toggleClass('hidden js-item');
					img.toggleClass('hidden').removeAttr('style');
					img.imagesLoaded().progress(function() {
						prodsCont.isotope('appended', th);
					});
				});
				if (!prodsCont.find('li.hidden').length) {
					btn.fadeOut(300);
				}
			}
			// console.log($moodGrid);
		});
		// if ($('#swimsuitsContainer').length > 0) {
        //
		// 	// $container = $('.b-isotope-grid');
        //
		// 	// init Isotope
		// 	// $swgrid = $('#swimsuitsContainer').isotope({
		// 	// 	itemSelector: '.grid-item',
		// 	// 	// percentPosition: true,
		// 	// 	masonry: {
		// 	// 		columnWidth: '.grid-sizer',
		// 	// 		// gutter: '.gutter-sizer'
		// 	// 	}
		// 	// });
		// 	// layout Isotope after each image loads
		// 	$swgrid.imagesLoaded().progress(function() {
		// 		$swgrid.isotope('layout');
		// 	});
        //
		// 	// filter items when filter link is clicked
		// 	// $('.b-isotope-filter a').on('click', function() {
		// 	// 	var selector = $(this).attr('data-filter');
		// 	// 	$container.isotope({
		// 	// 		filter: selector
		// 	// 	});
		// 	// 	return false;
		// 	// });
		// 	//
		// 	// $('.b-isotope-filter a').on('click', function() {
		// 	// 	$('.b-isotope-filter').find('.current').removeClass('current');
		// 	// 	$(this).parent().addClass('current');
		// 	// });
		// }




		/////////////////////////////////////
		// Select customization
		/////////////////////////////////////

		if ($('.selectpicker').length > 0) {

			$('.selectpicker').selectpicker({
				style: 'ui-select'
			});
		}



		////////////////////////////////////////////
		// single product slider
		///////////////////////////////////////////

		// var zoomFunc=function(pict, img){
		// 		pict.fadeOut(300, function() {
		// 			pict.parent().find('.dynamic').fadeIn().removeClass('xs-hidden');
		// 		});
        //
		// 		// picture=img;
		// 		// Initialize plugin (with custom event)
		// 		img.guillotine({
		// 			eventOnChange: 'guillotinechange',
		// 			init: { scale: 0.4 }
		// 		});
		// 		// img.guillotine('fit');
		// 		// img.guillotine({x: 0, y: 0});
        //
		// 		// Display inital data
		// 		var data = img.guillotine('getData');
		// 		// console.log(data);
		// 		for (var key in data) { $('#' + key).html(data[key]); }
        //
		// 		// Bind button actions
		// 		// $('#rotate_left').click(function() { picture.guillotine('rotateLeft'); });
		// 		// $('#rotate_right').click(function() { picture.guillotine('rotateRight'); });
		// 		// $('#fit').click(funcrtion() { picture.guillotine('fit'); });
		// 		$('.js-zoom_in').click(function() { img.guillotine('zoomIn'); });
		// 		$('.js-zoom_out').click(function() { img.guillotine('zoomOut'); });
        //
		// 		// Update data on change
		// 		img.on('guillotinechange', function(ev, data, action) {
		// 			data.scale = parseFloat(data.scale.toFixed(1));
		// 			for (var k in data) { $('#' + k).html(data[k]); }
		// 		});
		// 		if (img.prop('complete')) img.trigger('load')
		// 	},
		// 	zoomInit=function(sel){
		// 		// console.log('init zoom');
		// 		// var picture = $(sel);
		// 		var picture = sel;
		// 		// console.log(picture);
		// 		var img = $('<img class="dynamic xs-hidden">'); //Equivalent: $(document.createElement('img'))
		// 		img.attr('src', picture.data('large_image'));
		// 		picture.parent().append(img);
		// 		// img.one('load', function(){
		// 		// 	console.log(img);
		// 		// })
		// 		// Make sure the image is completely loaded before calling the plugin
		// 		img.one('load', function() {
		// 			zoomFunc(picture, img);
		// 		});
        //
		// 		// Make sure the 'load' event is triggered at least once (for cached images)
		// 		if (picture.prop('complete')) picture.trigger('load')
		// 	};

		// if($('.js-prodslider').length){
		// 	var slider=$('.js-prodslider'), args;
		// 	if (document.documentElement.clientWidth>992) {
		// 		args={
		// 			width: '100%',
		// 			height: 650,
		// 			fade: true,
		// 			arrows: true,
		// 			buttons: false,
		// 			fullScreen: true,
		// 			// autoHeight: true,
		// 			// shuffle: true,
		// 			// smallSize: 500,
		// 			// mediumSize: 1000,
		// 			// largeSize: 3000,
		// 			// thumbnailArrows: false,
		// 			autoplay: false,
		// 			// init: function(event){
		// 			// 	zoomFunc('.sp-slide.sp-selected img');
		// 			// }
		// 		};
		// 	} else {
		// 		args={
		// 			width: '100%',
		// 			// height: 650,
		// 			fade: true,
		// 			arrows: true,
		// 			buttons: false,
		// 			fullScreen: true,
		// 			autoHeight: true,
		// 			// shuffle: true,
		// 			// smallSize: 500,
		// 			// mediumSize: 1000,
		// 			// largeSize: 3000,
		// 			// thumbnailArrows: false,
		// 			autoplay: false,
		// 			// init: function(event){
		// 			// 	zoomFunc('.sp-slide.sp-selected img');
		// 			// }
		// 		};
		// 	}
		// 	slider.sliderPro(args);
		// 	$(document).on('click', '.sp-slide.sp-selected .img-controls.ready button', function(event){
		// 		zoomInit($(this).parents('.sp-selected').find('img'));
		// 		$(this).parent().removeClass('ready');
		// 	});
			// slider.on('init', function(event){
			// 	console.log(event);
			// })
		// }



		////////////////////////////////////////////
		// Main slider
		///////////////////////////////////////////


		if ($('#main-slider').length > 0) {

			// var sliderWidth = $("#main-slider").data("slider-width");
			// var sliderHeigth = $("#main-slider").data("slider-height");
			// var sliderArrows = $("#main-slider").data("slider-arrows");
			// var sliderButtons = $("#main-slider").data("slider-buttons");
			var mainSlidArgs;
			// if (document.documentElement.clientWidth>768) {
				mainSlidArgs={
					width: $("#main-slider").data("slider-width"),
					height: $("#main-slider").data("slider-height"),
					arrows: $("#main-slider").data("slider-arrows"),
					buttons: $("#main-slider").data("slider-buttons"),
					fade: true,
					fullScreen: true,
					touchSwipe: false,
					autoplay: false,
					loop: true
				};
			// } else {
			// 	mainSlidArgs={
			// 		width: $("#main-slider").data("slider-width"),
			// 		height: $("#main-slider").data("slider-height"),
			// 		// arrows: false,
			// 		buttons: $("#main-slider").data("slider-buttons"),
			// 		fade: true,
			// 		fullScreen: true,
			// 		touchSwipe: false,
			// 		autoplay: false,
			// 		loop: true
			// 	};
			// }

			$('#main-slider').sliderPro(mainSlidArgs);
		}

		/* Typed cursor */


		// $(".typed-strings").typed({
		// 	stringsElement: $('.typed-strings ul'),
		// 	typeSpeed: 30,
		// 	backDelay: 500,
		// 	loop: false,
		// 	contentType: 'html', // or text
		// 	// defaults to false for infinite loop
		// 	loopCount: false,
		// 	resetCallback: function() {
		// 		newTyped();
		// 	}
		// });
		//
		//
		// $(".typed-strings2").typed({
		// 	stringsElement: $('.typed-strings2 ul'),
		// 	typeSpeed: 30,
		// 	backDelay: 500,
		// 	loop: false,
		// 	contentType: 'html', // or text
		// 	// defaults to false for infinite loop
		// 	loopCount: true,
		// 	resetCallback: function() {
		// 		newTyped();
		// 	}
		// });




		/////////////////////////////////////
		//  RevealFX Start
		/////////////////////////////////////




		for (var i = 1; i < 9; i++) {
			var el = 'revealfx' + i;
			// console.log(el);
			// $(el).waypoint(function(){
			// 	// console.log(this.element);
			// 	var rev1 = new RevealFx(
			// 		this.element, {
			// 			revealSettings: {
			// 				bgcolor: '#000',
			// 				onCover: function(contentEl, revealerEl) {
			// 					contentEl.style.opacity = 1;
			// 				}
			// 			}
			// 		}
			// 	);
			// 	rev1.reveal();
			// }, {
			// 	triggerOnce: true,
			// 	offset: '95%'
			// });
			// console.log(document.getElementById(el));
			if (document.getElementById(el)) {
				var waypoint = new Waypoint({
					element: document.getElementById(el),
					handler: function(direction) {
						// notify(this.element.id + ' triggers at ' + this.triggerPoint)
						var rev1 = new RevealFx(
							this.element, {
								revealSettings: {
									bgcolor: '#000',
									onCover: function(contentEl, revealerEl) {
										contentEl.style.opacity = 1;
									}
								}
							}
						);
						rev1.reveal();
						this.destroy();
					},
					offset: '95%'
				})
				// waypoint.destroy();
			}
		}
		// $('#revealfx1').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx1'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		//
		//
		// $('#revealfx2').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx2'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		// $('#revealfx3').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx3'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		// $('#revealfx4').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx4'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		//
		//
		//
		//
		//
		// $('#revealfx5').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx5'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		//
		//
		//
		// $('#revealfx6').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx6'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		//
		//
		//
		//
		// $('#revealfx7').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx7'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		//
		//
		//
		// $('#revealfx8').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx8'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });
		//
		//
		// $('#revealfx8').waypoint(function() {
		//
		//
		//
		// 	var rev1 = new RevealFx
		//
		// 		(
		//
		//
		// 		document.querySelector('#revealfx8'), {
		// 			revealSettings: {
		// 				bgcolor: '#000',
		// 				onCover: function(contentEl, revealerEl) {
		// 					contentEl.style.opacity = 1;
		// 				}
		// 			}
		//
		// 		}
		//
		// 	);
		//
		// 	rev1.reveal();
		//
		//
		// }, {
		// 	triggerOnce: true,
		// 	offset: '95%'
		// });


		/////////////////////////////////////
		//  Effect Active
		/////////////////////////////////////






		$('.effect-active').waypoint(function() {


			$(this).addClass("active");


		}, {
			triggerOnce: true,
			offset: '55%'
		});

		$('.effect-active').on("hover", function() {
			$(this).removeClass("active");
		});









		/////////////////////////////////////////////////////////////////
		// OWL Sliders
		/////////////////////////////////////////////////////////////////

		var Core = {

			initialized: false,

			initialize: function() {

				if (this.initialized) return;
				this.initialized = true;

				this.build();

			},

			build: function() {

				// Owl Carousel

				this.initOwlCarousel();
			},
			initOwlCarousel: function(options) {

				$(".enable-owl-carousel").each(function(i) {
					var $owl = $(this);
					// console.log('owl enable');

					var itemsData = $owl.data('items');
					var navigationData = $owl.data('navigation');
					var paginationData = $owl.data('pagination');
					var singleItemData = $owl.data('single-item');
					var autoPlayData = $owl.data('auto-play');
					var transitionStyleData = $owl.data('transition-style');
					var mainSliderData = $owl.data('main-text-animation');
					var afterInitDelay = $owl.data('after-init-delay');
					var stopOnHoverData = $owl.data('stop-on-hover');
					// var min480 = $owl.data('min480');
					// var min768 = $owl.data('min768');
					// var min992 = $owl.data('min992');
					// var min1200 = $owl.data('min1200');
					// var owlArgs={
					// 	navigation: navigationData,
					// 	pagination: paginationData,
					// 	singleItem: singleItemData,
					// 	autoPlay: autoPlayData,
					// 	transitionStyle: transitionStyleData,
					// 	stopOnHover: stopOnHoverData,
					// 	navigationText: ["<i></i>", "<i></i>"],
					// 	items: itemsData,
					// 	itemsCustom: [
					// 		[0, 1],
					// 		[465, min480],
					// 		[750, min768],
					// 		[975, min992],
					// 		[1185, min1200]
					// 	],
					// 	afterInit: function(elem) {
					// 		if (mainSliderData) {
					// 			setTimeout(function() {
					// 				$('.main-slider_zoomIn').css('visibility', 'visible').removeClass('zoomIn').addClass('zoomIn');
					// 				$('.main-slider_fadeInLeft').css('visibility', 'visible').removeClass('fadeInLeft').addClass('fadeInLeft');
					// 				$('.main-slider_fadeInLeftBig').css('visibility', 'visible').removeClass('fadeInLeftBig').addClass('fadeInLeftBig');
					// 				$('.main-slider_fadeInRightBig').css('visibility', 'visible').removeClass('fadeInRightBig').addClass('fadeInRightBig');
					// 			}, afterInitDelay);
					// 		}
					// 	},
					// 	beforeMove: function(elem) {
					// 		if (mainSliderData) {
					// 			$('.main-slider_zoomIn').css('visibility', 'hidden').removeClass('zoomIn');
					// 			$('.main-slider_slideInUp').css('visibility', 'hidden').removeClass('slideInUp');
					// 			$('.main-slider_fadeInLeft').css('visibility', 'hidden').removeClass('fadeInLeft');
					// 			$('.main-slider_fadeInRight').css('visibility', 'hidden').removeClass('fadeInRight');
					// 			$('.main-slider_fadeInLeftBig').css('visibility', 'hidden').removeClass('fadeInLeftBig');
					// 			$('.main-slider_fadeInRightBig').css('visibility', 'hidden').removeClass('fadeInRightBig');
					// 		}
					// 	},
					// 	afterMove: sliderContentAnimate,
					// 	afterUpdate: sliderContentAnimate,
					// };
					var owlArgs={
						nav: navigationData,
						dots: paginationData,
						// singleItem: singleItemData,
						autoplay: autoPlayData,
						// transitionStyle: transitionStyleData,
						autoplayHoverPause: stopOnHoverData,
						navText: ["<i></i>", "<i></i>"],
						items: itemsData,
						// itemsCustom: [
						// 	[0, 1],
						// 	[465, min480],
						// 	[750, min768],
						// 	[975, min992],
						// 	[1185, min1200]
						// ],
						onInitialized: function(elem) {
							if (mainSliderData) {
								setTimeout(function() {
									$('.main-slider_zoomIn').css('visibility', 'visible').removeClass('zoomIn').addClass('zoomIn');
									$('.main-slider_fadeInLeft').css('visibility', 'visible').removeClass('fadeInLeft').addClass('fadeInLeft');
									$('.main-slider_fadeInLeftBig').css('visibility', 'visible').removeClass('fadeInLeftBig').addClass('fadeInLeftBig');
									$('.main-slider_fadeInRightBig').css('visibility', 'visible').removeClass('fadeInRightBig').addClass('fadeInRightBig');
								}, afterInitDelay);
							}
						},
						onTranslate: function(elem) {
							if (mainSliderData) {
								$('.main-slider_zoomIn').css('visibility', 'hidden').removeClass('zoomIn');
								$('.main-slider_slideInUp').css('visibility', 'hidden').removeClass('slideInUp');
								$('.main-slider_fadeInLeft').css('visibility', 'hidden').removeClass('fadeInLeft');
								$('.main-slider_fadeInRight').css('visibility', 'hidden').removeClass('fadeInRight');
								$('.main-slider_fadeInLeftBig').css('visibility', 'hidden').removeClass('fadeInLeftBig');
								$('.main-slider_fadeInRightBig').css('visibility', 'hidden').removeClass('fadeInRightBig');
							}
						},
						afterMove: sliderContentAnimate,
						afterUpdate: sliderContentAnimate,
					};

					$owl.owlCarousel(owlArgs);


					// owlArgs.afterInit=function(elem){
					// 	$(elem).find('.owl-controls').appendTo('.js-about-nav');
					// 	if ($(elem).find('.js-adv-holder').length) {
					// 		$(elem).find('.js-adv-holder .grid').each(function(index) {
					// 			$(this).isotope({
					// 				itemSelector: '.grid-item',
					// 				// percentPosition: true,
					// 				masonry: {
					// 					// columnWidth: '.grid-sizer',
					// 					gutter: '.gutter-sizer'
					// 				}
					// 			});
					// 		});
					// 	}
					// };
					// owlArgs.onInitialized=function(elem){
					// 	$(elem).find('.owl-controls').appendTo('.js-about-nav');
					// 	if ($(elem).find('.js-adv-holder').length) {
					// 		$(elem).find('.js-adv-holder .grid').each(function(index) {
					// 			$(this).isotope({
					// 				itemSelector: '.grid-item',
					// 				// percentPosition: true,
					// 				masonry: {
					// 					// columnWidth: '.grid-sizer',
					// 					gutter: '.gutter-sizer'
					// 				}
					// 			});
					// 		});
					// 	}
					// };
					$('.js-about-carousel').owlCarousel(owlArgs);

				});

				function sliderContentAnimate(elem) {
					var $elem = elem;
					var afterMoveDelay = $elem.data('after-move-delay');
					var mainSliderData = $elem.data('main-text-animation');
					if (mainSliderData) {
						setTimeout(function() {
							$('.main-slider_zoomIn').css('visibility', 'visible').addClass('zoomIn');
							$('.main-slider_slideInUp').css('visibility', 'visible').addClass('slideInUp');
							$('.main-slider_fadeInLeft').css('visibility', 'visible').addClass('fadeInLeft');
							$('.main-slider_fadeInRight').css('visibility', 'visible').addClass('fadeInRight');
							$('.main-slider_fadeInLeftBig').css('visibility', 'visible').addClass('fadeInLeftBig');
							$('.main-slider_fadeInRightBig').css('visibility', 'visible').addClass('fadeInRightBig');
						}, afterMoveDelay);
					}
				}

                // product slider
				if ($('.js-prodslider').length) {
					var thumbPrevInd,
						prodslider=$('.js-prodslider'),
						prodThumbs=$('.js-owl-thumbs'),
						thumbArgs={
							// thumbs: false,
							nav: false,
							dots: false,
							autoplay: false,
							mouseDrag: true,
							touchDrag: true,
							pullDrag: false,
							freeDrag: false,
							// link: '.js-prodslider',
							// loop: true,
							center: true,
							onInitialized: function(elem) {
								// console.log(elem);
								$(prodThumbs.find('.owl-item')[0]).addClass('curr');
							},
							onTranslated: function(elem){
								// console.log(elem);
								// console.log($(elem).find('img'));
								// console.log(elem.item.index);
								prodslider.trigger('to.owl.carousel', [elem.item.index]);
							}
						},
						prodArgs={
							// thumbs: false,
							// thumbImage: false,
							// thumbsPrerendered: false,
							// thumbContainerClass: 'js-owl-thumbs',
							nav: true,
							dots: false,
							// singleItem: singleItemData,
							autoplay: false,
							// transitionStyle: transitionStyleData,
							// autoplayHoverPause: stopOnHoverData,
							// navText: ["<i></i>", "<i></i>"],
							navText: ["<div class=\"sp-arrow sp-previous-arrow\"></div>", "<div class=\"sp-arrow sp-next-arrow\"></div>"],
							// navText: ["<i class=\"fa fa-angle-double-left\" aria-hidden=\"true\"></i>", "<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>"],
							items: 1,
							// link: '.js-owl-thumbs',
							// loop: true,
							// center: true,
							// itemsCustom: [
							// 	[0, 1],
							// 	[465, min480],
							// 	[750, min768],
							// 	[975, min992],
							// 	[1185, min1200]
							// ],
							// onInitialized: function(elem) {
							// 	// console.log(elem);
							// 	// prodThumbs.find('.owl-item').addClass('curr');
							// },
							onTranslated: function(elem){
								// console.log(elem);
								// console.log($(elem).find('img'));
								// console.log(elem.item);
								gotoThumb(elem.item.index);
								// prodThumbs.trigger('to.owl.carousel', [elem.item.index]);
								// console.log(prodThumbs.find('.owl-item')[elem.item.index]);
								prodThumbs.find('.owl-item.curr').toggleClass('curr');
								$(prodThumbs.find('.owl-item')[elem.item.index]).toggleClass('curr');
								// prodThumbs.find('.owl-item')[elem.item.index].addClass('curr');
							}
						},
						thumbitems,
						gotoThumb=function(ind){
							prodThumbs.trigger('to.owl.carousel', [ind]);
						};
					switch (true) {
						case (document.documentElement.clientWidth<=768):
							thumbitems=3;
							break;
						default:
							thumbitems=5
					}
					prodslider.owlCarousel(prodArgs);
					thumbArgs.items=thumbitems;
					prodThumbs.owlCarousel(thumbArgs);
					// Custom Navigation Events
					prodThumbs.find('img').on('click', function(event){
						event.preventDefault();
						var n = $(this).closest('.owl-item').index();
						// console.log(event);
						// console.log(n);
						gotoThumb(n);
					});
					prodslider.lightGallery({
						selector: '.js-item img',
						thumbnail:false,
						zoom: true,
						autoplay: false,
						hash: false,
						getCaptionFromTitleOrAlt: false,
						share: false
					});
					prodslider.on('onBeforePrevSlide.lg onBeforeNextSlide.lg onBeforeSlide.lg',function(event, prevIndex, index, fromTouch, fromThumb){
						if (typeof prevIndex!=='undefined') {
							console.log(prevIndex, index, fromTouch, fromThumb);
						} else {
							console.log(index, fromTouch, fromThumb);
						}

						// var newind=(prevIndex>index)?index:index+1;
						if (fromTouch||fromThumb) {
							gotoThumb(index);
						}
					});
					// prodslider.on('onBeforeSlide.lg',function(event, prevIndex, index, fromTouch, fromThumb){
					// 	if (typeof prevIndex!=='undefined') {
					// 		console.log(prevIndex, index, fromTouch, fromThumb);
					// 	} else {
					// 		console.log(index, fromTouch, fromThumb);
					// 	}
                    //
					// 	var newind=(prevIndex>index)?index:index+1;
					// 	if (fromTouch||fromThumb) {
					// 		gotoThumb(newind);
					// 	}
					// });
				}
			},

		};

		Core.initialize();

	});

}(this, jQuery));

////////////////////////////////////////////
// Image animation
///////////////////////////////////////////

(function() {
	var tiltSettings = [{}, {
		movement: {
			imgWrapper: {
				translation: {
					x: 10,
					y: 10,
					z: 30
				},
				rotation: {
					x: 0,
					y: -10,
					z: 0
				},
				reverseAnimation: {
					duration: 200,
					easing: 'easeOutQuad'
				}
			},
			lines: {
				translation: {
					x: 10,
					y: 10,
					z: [0, 70]
				},
				rotation: {
					x: 0,
					y: 0,
					z: -2
				},
				reverseAnimation: {
					duration: 2000,
					easing: 'easeOutExpo'
				}
			},
			caption: {
				rotation: {
					x: 0,
					y: 0,
					z: 2
				},
				reverseAnimation: {
					duration: 200,
					easing: 'easeOutQuad'
				}
			},
			overlay: {
				translation: {
					x: 10,
					y: -10,
					z: 0
				},
				rotation: {
					x: 0,
					y: 0,
					z: 2
				},
				reverseAnimation: {
					duration: 2000,
					easing: 'easeOutExpo'
				}
			},
			shine: {
				translation: {
					x: 100,
					y: 100,
					z: 0
				},
				reverseAnimation: {
					duration: 200,
					easing: 'easeOutQuad'
				}
			}
		}
	}, {
		movement: {
			imgWrapper: {
				rotation: {
					x: -5,
					y: 10,
					z: 0
				},
				reverseAnimation: {
					duration: 900,
					easing: 'easeOutCubic'
				}
			},
			caption: {
				translation: {
					x: 30,
					y: 30,
					z: [0, 40]
				},
				rotation: {
					x: [0, 15],
					y: 0,
					z: 0
				},
				reverseAnimation: {
					duration: 1200,
					easing: 'easeOutExpo'
				}
			},
			overlay: {
				translation: {
					x: 10,
					y: 10,
					z: [0, 20]
				},
				reverseAnimation: {
					duration: 1000,
					easing: 'easeOutExpo'
				}
			},
			shine: {
				translation: {
					x: 100,
					y: 100,
					z: 0
				},
				reverseAnimation: {
					duration: 900,
					easing: 'easeOutCubic'
				}
			}
		}
	}, {
		movement: {
			imgWrapper: {
				rotation: {
					x: -5,
					y: 10,
					z: 0
				},
				reverseAnimation: {
					duration: 50,
					easing: 'easeOutQuad'
				}
			},
			caption: {
				translation: {
					x: 20,
					y: 20,
					z: 0
				},
				reverseAnimation: {
					duration: 200,
					easing: 'easeOutQuad'
				}
			},
			overlay: {
				translation: {
					x: 5,
					y: -5,
					z: 0
				},
				rotation: {
					x: 0,
					y: 0,
					z: 6
				},
				reverseAnimation: {
					duration: 1000,
					easing: 'easeOutQuad'
				}
			},
			shine: {
				translation: {
					x: 50,
					y: 50,
					z: 0
				},
				reverseAnimation: {
					duration: 50,
					easing: 'easeOutQuad'
				}
			}
		}
	}, {
		movement: {
			imgWrapper: {
				translation: {
					x: 0,
					y: -8,
					z: 0
				},
				rotation: {
					x: 3,
					y: 3,
					z: 0
				},
				reverseAnimation: {
					duration: 1200,
					easing: 'easeOutExpo'
				}
			},
			lines: {
				translation: {
					x: 15,
					y: 15,
					z: [0, 15]
				},
				reverseAnimation: {
					duration: 1200,
					easing: 'easeOutExpo'
				}
			},
			overlay: {
				translation: {
					x: 0,
					y: 8,
					z: 0
				},
				reverseAnimation: {
					duration: 600,
					easing: 'easeOutExpo'
				}
			},
			caption: {
				translation: {
					x: 10,
					y: -15,
					z: 0
				},
				reverseAnimation: {
					duration: 900,
					easing: 'easeOutExpo'
				}
			},
			shine: {
				translation: {
					x: 50,
					y: 50,
					z: 0
				},
				reverseAnimation: {
					duration: 1200,
					easing: 'easeOutExpo'
				}
			}
		}
	}, {
		movement: {
			lines: {
				translation: {
					x: -5,
					y: 5,
					z: 0
				},
				reverseAnimation: {
					duration: 1000,
					easing: 'easeOutExpo'
				}
			},
			caption: {
				translation: {
					x: 15,
					y: 15,
					z: 0
				},
				rotation: {
					x: 0,
					y: 0,
					z: 3
				},
				reverseAnimation: {
					duration: 1500,
					easing: 'easeOutElastic',
					elasticity: 700
				}
			},
			overlay: {
				translation: {
					x: 15,
					y: -15,
					z: 0
				},
				reverseAnimation: {
					duration: 500,
					easing: 'easeOutExpo'
				}
			},
			shine: {
				translation: {
					x: 50,
					y: 50,
					z: 0
				},
				reverseAnimation: {
					duration: 500,
					easing: 'easeOutExpo'
				}
			}
		}
	}, {
		movement: {
			imgWrapper: {
				translation: {
					x: 5,
					y: 5,
					z: 0
				},
				reverseAnimation: {
					duration: 800,
					easing: 'easeOutQuart'
				}
			},
			caption: {
				translation: {
					x: 10,
					y: 10,
					z: [0, 50]
				},
				reverseAnimation: {
					duration: 1000,
					easing: 'easeOutQuart'
				}
			},
			shine: {
				translation: {
					x: 50,
					y: 50,
					z: 0
				},
				reverseAnimation: {
					duration: 800,
					easing: 'easeOutQuart'
				}
			}
		}
	}, {
		movement: {
			lines: {
				translation: {
					x: 40,
					y: 40,
					z: 0
				},
				reverseAnimation: {
					duration: 1500,
					easing: 'easeOutElastic'
				}
			},
			caption: {
				translation: {
					x: 20,
					y: 20,
					z: 0
				},
				rotation: {
					x: 0,
					y: 0,
					z: -5
				},
				reverseAnimation: {
					duration: 1000,
					easing: 'easeOutExpo'
				}
			},
			overlay: {
				translation: {
					x: -30,
					y: -30,
					z: 0
				},
				rotation: {
					x: 0,
					y: 0,
					z: 3
				},
				reverseAnimation: {
					duration: 750,
					easing: 'easeOutExpo'
				}
			},
			shine: {
				translation: {
					x: 100,
					y: 100,
					z: 0
				},
				reverseAnimation: {
					duration: 750,
					easing: 'easeOutExpo'
				}
			}
		}
	}];

	function init() {
		var idx = 0;
		[].slice.call(document.querySelectorAll('a.tilter')).forEach(function(el, pos) {
			idx = pos % 2 === 0 ? idx + 1 : idx;
			new TiltFx(el, tiltSettings[idx - 1]);
		});
	}

	// open linked images in magnific popup
	$('.js-image-link').magnificPopup();

	// Preload all images.
	imagesLoaded(document.querySelector('body'), function() {
		// document.body.classList.remove('loading');
		init();
	});

})();

/*!
 * Version: 1.2 Development
 */



(function() {
	"use strict";

	var Core = {
		initialized: false,
		initialize: function() {
			if (this.initialized)
				return;
			this.initialized = true;
			this.build();
		},
		build: function() {

			this.fixedHeader();
			// Init toggle menu
			this.initToggleMenu();
			// Search
			// this.initSearchModal();
			// Dropdown menu
			this.dropdownhover();

		},




		// initSearchModal: function(options) {
        //
        //
		// 	$(document).on("click", ".btn_header_search", function(event) {
		// 		event.preventDefault();
        //
		// 		$(".header-search").addClass("open");
		// 	});
		// 	$(document).on("click", ".search-form_close , .search-close", function(event) {
		// 		event.preventDefault();
		// 		$(".header-search").removeClass("open");
		// 	});
        //
		// },




		initToggleMenu: function() {
			$('.toggle-menu-button').each(function(i) {
				var trigger = $(this);
				var isClosed = true;

				function showMenu() {
					$('#nav').addClass('navbar-scrolling-fixing');
					if (trigger.hasClass("js-toggle-screen")) {
						$('#fixedMenu').delay(0).fadeIn(300);
					}

					trigger.addClass('is-open');
					isClosed = false;
				}

				$('.fullmenu-close').on('click', function(e) {
					e.preventDefault();
					if (isClosed === true) {
						hideMenu();
					} else {
						hideMenu();
					}
				});

				function hideMenu() {
					$('#fixedMenu').fadeOut(100);
					$('#nav').removeClass('navbar-scrolling-fixing');
					trigger.removeClass('is-open');
					isClosed = true;
				}

				trigger.on('click', function(e) {
					e.preventDefault();
					if (isClosed === true) {
						showMenu();
					} else {
						hideMenu();
					}
				});


			});
		},



		dropdownhover: function(options) {
			/** Extra script for smoother navigation effect **/
			if ($(window).width() > 798) {
				$('.yamm').on('mouseenter', '.navbar-nav > .dropdown', function() {
					"use strict";
					$(this).addClass('open');
				}).on('mouseleave', '.navbar-nav > .dropdown', function() {
					"use strict";
					$(this).removeClass('open');
				});
			}
		},

		fixedHeader: function(options) {
			// if ($(window).width() > 767) {
				// Fixed Header
				var topOffset = $(window).scrollTop();
				if (topOffset > 0) {
					$('.header').addClass('navbar-scrolling');
				}
				$(window).on('scroll', function() {
					var fromTop = $(this).scrollTop();
					if (fromTop > 0) {
						$('body').addClass('fixed-header');
						$('.header').addClass('navbar-scrolling');
					} else {
						$('body').removeClass('fixed-header');
						$('.header').removeClass('navbar-scrolling');
					}

				});
			// }
		},
	};
	Core.initialize();




	/////////////////////////////////////////////////////////////////
	//   Dropdown Menu Fade
	/////////////////////////////////////////////////////////////////




	$(".yamm >li").hover(
		function() {
			$('.dropdown-menu', this).fadeIn("fast");
		},
		function() {
			$('.dropdown-menu', this).fadeOut("fast");
		}
	);




	window.prettyPrint && prettyPrint();
	$(document).on('click', '.yamm .dropdown-menu', function(event) {
		// event.stopPropagation();
		// console.log(event);
		event.preventDefault();
	});




	// Create a new instance of Slidebars
	var controller = new slidebars();
	//
	// Events
	$( controller.events ).on( 'init', function () {
		//console.log( 'Init event' );
	} );

	$( controller.events ).on( 'exit', function () {
		//console.log( 'Exit event' );
	} );

	$( controller.events ).on( 'css', function () {
		//console.log( 'CSS event' );
	} );

	$( controller.events ).on( 'opening', function ( event, id ) {
		//console.log( 'Opening event of slidebar with id ' + id );
	} );

	$( controller.events ).on( 'opened', function ( event, id ) {
		//console.log( 'Opened event of slidebar with id ' + id );
	} );

	$( controller.events ).on( 'closing', function ( event, id ) {
		//console.log( 'Closing event of slidebar with id ' + id );
	} );

	$( controller.events ).on( 'closed', function ( event, id ) {
		//console.log( 'Closed event of slidebar with id ' + id );
	} );

	// Initialize Slidebars
	controller.init();



	  // Mobile Slidebar controls



	  $( '.js-toggle-mobile-slidebar' ).on( 'click', function ( event ) {
			event.stopPropagation();
			controller.toggle( 'mobile-slidebar' );
		} );





		// Panel nav  Slidebar controls



		 $( '.js-open-slidebar-panel-left' ).on( 'click', function ( event ) {
	  event.preventDefault();
	  event.stopPropagation();
	  controller.toggle( 'slidebar-panel-left' );
	} );





	// Left Slidebar controls
	$( '.js-open-left-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.open( 'slidebar-1' );
	} );

	$( '.js-close-left-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.close( 'slidebar-1' );
	} );

	$( '.js-toggle-left-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.toggle( 'slidebar-1' );
	} );

	// Right Slidebar controls
	$( '.js-open-right-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.open( 'slidebar-2' );
	} );

	$( '.js-close-right-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.close( 'slidebar-2' );
	} );

	$( '.js-toggle-right-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.toggle( 'slidebar-2' );
	} );

	// Top Slidebar controls
	$( '.js-open-top-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.open( 'slidebar-3' );
	} );

	$( '.js-close-top-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.close( 'slidebar-3' );
	} );

	$( '.js-toggle-top-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.toggle( 'slidebar-3' );
	} );

	// Bottom Slidebar controls
	$( '.js-open-bottom-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.open( 'slidebar-4' );
	} );

	$( '.js-close-bottom-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.close( 'slidebar-4' );
	} );

	$( '.js-toggle-bottom-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.toggle( 'slidebar-4' );
	} );

	// Close any
	$( controller.events ).on( 'opened', function () {
		$( '[data-canvas="container"]' ).addClass( 'js-close-any-slidebar' );
	$( '.toggle-menu-button' ).addClass( 'is-open' );
	} );

	$( controller.events ).on( 'closed', function () {
		$( '[data-canvas="container"]' ).removeClass( 'js-close-any-slidebar' );
	$( '.toggle-menu-button' ).removeClass( 'is-open' );
	} );

	$( 'body' ).on( 'click', '.js-close-any-slidebar', function ( event ) {
		event.stopPropagation();
		controller.close();
	} );

	// Initilize, exit and css reset
	$( '.js-initialize-slidebars' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.init();
	} );

	$( '.js-exit-slidebars' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.exit();
	} );

	$( '.js-reset-slidebars-css' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.css();
	} );

	// Is and get
	$( '.js-is-active' ).on( 'click', function ( event ) {
		event.stopPropagation();
		//console.log( controller.isActive() );
	} );

	$( '.js-is-active-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		var id = prompt( 'Enter a Slidebar id' );
		//console.log( controller.isActiveSlidebar( id ) );
	} );

	$( '.js-get-active-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		//console.log( controller.getActiveSlidebar() );
	} );

	$( '.js-get-all-slidebars' ).on( 'click', function ( event ) {
		event.stopPropagation();
		//console.log( controller.getSlidebars() );

	} );

	$( '.js-get-slidebar' ).on( 'click', function ( event ) {
		event.stopPropagation();
		var id = prompt( 'Enter a Slidebar id' );
		//console.log( controller.getSlidebar( id ) );
	} );

	// Callbacks
	$( '.js-init-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.init( function () {
			//console.log( 'Init callback' );
		} );
	} );

	$( '.js-exit-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.exit( function () {
			//console.log( 'Exit callback' );
		} );
	} );

	$( '.js-css-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.css( function () {
			//console.log( 'CSS callback' );
		} );
	} );

	$( '.js-open-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.open( 'slidebar-1', function () {
			//console.log( 'Open callback' );
		} );
	} );

	$( '.js-close-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.close( function () {
			//console.log( 'Close callback' );
		} );
	} );

	$( '.js-toggle-callback' ).on( 'click', function ( event ) {
		event.stopPropagation();
		controller.toggle( 'slidebar-1', function () {
			//console.log( 'Toggle callback' );
		} );
	} );



})();
var player;
function ytimg(){
	var div, n,
		v = document.getElementsByClassName("youtube-player");
	for (n = 0; n < v.length; n++) {
		div = document.createElement("div");
		div.id=v[n].dataset.id;
		div.setAttribute("data-id", v[n].dataset.id);
		div.innerHTML = labnolThumb(v[n].dataset.id);
		// div.onclick = (typeof(yaCounter44554915)!=='undefined') ? yaCounter44554915.reachGoal('click_video', function(){labnolIframe;}) : labnolIframe;
		div.onclick = labnolIframe;
		$('.js-play').get(0).onclick = labnolIframe;
		v[n].appendChild(div);
	}
}
function labnolThumb(id) {
	// var thumb = '<img data-src="https://i.ytimg.com/vi/ID/maxresdefault.jpg"><div class="play"></div>';
	// var thumb = '<img data-lazy="https://i.ytimg.com/vi/ID/maxresdefault.jpg">',
	// var play = '<div class="play"></div>';
	// if($('.testimonial-slider').length){
	// 	thumb = '<img data-lazy="https://i.ytimg.com/vi/ID/mqdefault.jpg">';
	// } else {
	// 	thumb = '<img src="https://i.ytimg.com/vi/ID/mqdefault.jpg">';
	// }
	// slider=($('.youtube-player[data-id="'+id+'"]').parent().hasClass('slide'))?'data-lazy':'data-src';
	slider='src';
	thumb=(typeof $('[data-id="'+id+'"]').data('poster')!=='undefined'&&$('[data-id="'+id+'"]').data('poster')!=='')?'<img '+slider+'="'+$('[data-id="'+id+'"]').data('poster')+'">' : '<img '+slider+'="https://i.ytimg.com/vi/'+id+'/maxresdefault.jpg"><div class="play"></div>';
	return thumb;
}
function labnolIframe() {
	// if($('.testimonial-slider').length){
	// 	var w=this.offsetWidth,
	// 		h=this.offsetHeight-6,
	// 		v_id=this.dataset.id,
	// 		params={
	// 			height: h,
	// 			width: w,
	// 			videoId: v_id,
	// 			events: {
	// 				'onReady': onPlayerReady
	// 			}
	// 		};
	// 	// document.querySelector('.testimonial-slider .prev').addEventListener('click', pauseVid);
	// 	// document.querySelector('.testimonial-slider .next').addEventListener('click', pauseVid);
	// } else {
		var w=$(this).parent().find('.youtube-player').get(0).offsetWidth,
			h=$(this).parent().find('.youtube-player').get(0).offsetHeight-6,
			v_id=$(this).parent().find('.youtube-player').get(0).dataset.id,
			params={
				height: h,
				width: w,
				videoId: v_id,
				events: {
					'onReady': onPlayerReady
				}
			};
	// }

	if(typeof YT === 'undefined'){
		var els = document.getElementsByTagName("script")[0],
			scp = document.createElement("script"),
			func = function () { els.parentNode.insertBefore(scp, els); };
		scp.type = "text/javascript";
		scp.id = 'youtube';
		scp.async = true;
		scp.src = "//www.youtube.com/iframe_api";
		if (window.opera == "[object Opera]") {
			document.addEventListener("DOMContentLoaded", f, false);
		} else { func(); }
		if (scp.readyState) { // IE, incl. IE9
			scp.onreadystatechange = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 500)
			}
			$('.js-play').fadeToggle(300, function() {
				//Stuff to do *after* the animation takes place
			})
		} else {
			scp.onload = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 500)
			}
			$('.js-play').fadeToggle(300, function() {
				//Stuff to do *after* the animation takes place
			})
		}
	} else {
		setTimeout(function(){
			player = new YT.Player(v_id, params);
		}, 500)
	}
}
function onPlayerReady(event) {
	event.target.playVideo();
}
function stopVideo() {
	player.stopVideo();
}
function pauseVid(event) {
	player.pauseVideo();
}
ytimg();

// custom variations selector
$(document).on('change', '.js-varselector input', function(event){
	// console.log(event.target);
	var val=event.target.value,
		av_vars=JSON.parse(document.querySelectorAll('.js-addToCart')[0].dataset.product_variations),
		c_var;
	// console.log(val);
	// console.log(av_vars);
	av_vars.forEach(function(item, i, arr){
		// console.log(item);
		if (item.attributes.attribute_size==val||item.attributes.attribute_pa_size==val) {
			c_var=item;
		}
	});
	// console.log(c_var);
	if (event.target.checked) {
		// var priceEl=$('.js-priceDisplay');
		document.querySelectorAll('.variation_id')[0].value=c_var.variation_id;
		if ($('.js-addToCart').parent().find('.amount').length>1) {
			$('.js-priceDisplay').text($('.js-addToCart').parent().find('.amount').first().text().charAt(0)+c_var.display_price);
		}
	} else {
		document.querySelectorAll('.variation_id')[0].value=0;
	}
});

// ajax add to cart
$(document).on('submit', '.js-addToCart', function(event){
	event.preventDefault();
	var form=$(this);
	form.find('[type=submit]').attr('disabled', 'disabled');
	// console.log(event);
	var result,
		v=(form.find('.variation_id').length) ? true : false,
		postData={
			prodid: form.find('[type=submit]').data('product_id'),
			quant: parseInt((form.find('.js-prodQty').length) ? form.find('.js-prodQty').val() : 1),
			variation: parseInt((v) ? form.find('.variation_id').val() : null)
		};
	// console.log(postData);
	if(v&&postData.variation==0) {
		form.find('[type=submit]').removeAttr('disabled');
		var errEl=form.find('.js-error');
		if (errEl.length&&!errEl.hasClass('triggered')) {
			// errEl.removeClass('hidden');
			errEl.slideToggle(300, function() {
				$(this).addClass('triggered');
			})
		}

		// throw new Error('Not permitted - choose variation!');
		return;
	};
	$.post({
		url: ajax_func.ajax_url,
		dataType: 'json',
		data: {
			action: 'generic_addToCart',
			params: postData
		}
	}).done(function(data){
		if (data.success==true) {
			// console.log(data.data);
			if (form.find('.error.triggered').length) {
				form.find('.error.triggered').slideToggle(300, function() {
					$(this).removeClass('triggered');
				})
			}
			updateCart();
			var html=data.data.html;
			// $('.popup-youtube').magnificPopup({
			// 	disableOn: 700,
			// 	type: 'inline',
			// 	mainClass: 'mfp-fade',
			// 	removalDelay: 160,
			// 	preloader: false,
			//
			// 	fixedContentPos: false
			// });
			$.magnificPopup.open({
				disableOn: 700,
				// type: 'inline',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false,
				items: {
					src: html, // can be a HTML string, jQuery object, or CSS selector
					type: 'inline'
				}
			});
			// $('body').append($.parseHTML(html)).css('overflow', 'hidden');
			// $(simpleModal).fadeIn(300);
			$(document).on('click', '.close, .fade', function(event){
				// $(this).closest('.modal').fadeOut(300, function(){$(this).remove()});
				// $('body').removeAttr('style');
				$.magnificPopup.close();
			});
		} else {
			console.log('error:', data);
		}
	});
	form.find('[type=submit]').removeAttr('disabled');
});

// ajax update cart after load
function updateCart(){
	$.post({
		url: ajax_func.ajax_url,
		dataType: 'json',
		data: {
			action: 'generic_ajax_cart',
			params: {}
		},
		success: function (data) {
			$('.js-cart-total').text(data.content);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(xhr.responseText);
		}
	});
}

// load more photos from instagram
$(document).on('click', '.js-morePhotos', function(event){
	event.preventDefault();
	if($('.js-instagram_photo').find('.hidden').length){
		var hid=$('.js-instagram_photo').find('.hidden').slice(0, 4);
		hid.fadeIn(300, function() {
			$(this).removeAttr('style').toggleClass('hidden js-item');
			$('.js-instagram-gallery').data('lightGallery').destroy(true);
			$('.js-instagram-gallery').lightGallery({
				selector: '.js-item',
				thumbnail: false,
				zoom: true,
				autoplay: false,
				hash: false,
				share: false
			});
		})

	} else {
		$(this).find('[type=submit]').attr('disabled', 'disabled');
		// console.log(event);
		var postData={url: $(this).data('url')};
		// console.log(postData);
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'generic_ajax_get_photos',
				params: postData
			},
			success: function (data) {
				// console.log(data);
				$('.js-instagram_photo').append($.parseHTML(data.data.content));
				zoomImg();
				if(data.data.hide===true){
					$('.js-morePhotos').fadeOut(300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
	}
});

var zoomImg=function(){
	$('.js-zoom-images').magnificPopup({
		type: 'image',
		mainClass: 'mfp-with-zoom', // this class is for CSS animation below

		zoom: {
			enabled: true, // By default it's false, so don't forget to enable it

			duration: 300, // duration of the effect, in milliseconds
			easing: 'ease-in-out', // CSS transition easing function

			// The "opener" function should return the element from which popup will be zoomed in
			// and to which popup will be scaled down
			// By defailt it looks for an image tag:
			opener: function(openerElement) {
				// openerElement is the element on which popup was initialized, in this case its <a> tag
				// you don't need to add "opener" option if this code matches your needs, it's defailt one.
				return openerElement.is('img') ? openerElement : openerElement.find('img');
			}
		}
	});
	// console.log('mfp fired');
};
function loadingHide() {
	// $('.loading').addClass("loading-hide");
	// $('.loading').fadeOut(500, function() {
	// 	//Stuff to do *after* the animation takes place
	// });
	$('.js-preloader').fadeOut(300, function() {
		setTimeout(function(){
			document.body.classList.remove('loading');
		}, 100)
	})
}

$(window).on('load', function(event) {
	// console.log(event);
	// var $preloader = $('#page-preloader'),
	// 	$spinner = $preloader.find('.spinner-loader');
	// $spinner.fadeOut();
	// $preloader.delay(50).fadeOut('slow');
	// setTimeout(screenerGo, 200);
	setTimeout(loadingHide, 200);
});
