<?php global $options; ?>
				<a href="#top" id="back-top"><span><i class="fa fa-angle-double-up" aria-hidden="true"></i></span></a>
				<div class="section-s-net text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="ui-title-block"><?=__('Stay Connected With Us !', 'giammetti')?></h2>
								<ul class="section-s-net__list list-inline">
									<li class="section-s-net__item"><a class="section-s-net__link" href="<?=(!empty($options['socl']['facebook']))?$options['socl']['facebook']:'https://www.facebook.com/giammettivienna/'?>">facebook</a></li>
									<li class="section-s-net__item"><a class="section-s-net__link" href="<?=(!empty($options['socl']['instagram']))?$options['socl']['instagram']:'https://www.instagram.com/giammettivienna/'?>">Instagram</a></li>
									<li class="section-s-net__item"><a class="section-s-net__link" href="<?=(!empty($options['socl']['vk']))?$options['socl']['vk']:'https://www.vk.com/giammettivienna/'?>">v kontakte</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</main>
			<footer class="footer">
				<div class="container">
					<div class="footer__main">
						<div class="row">
							<div class="col-md-6 col-sm-4 col-xs-6">
								<div class="footer-section">
									<?php
									$logo=(empty($options['gnrl']['header_logo_id'])) ? '<img src="'.$options['tpld'].'/assets/img/general/logo.png" alt="'.get_option( 'blogname' ).'" class="normal-logo">' : remove_width_attribute(wp_get_attachment_image( $options['gnrl']['header_logo_id'], 'full', false, array('class'=>'normal-logo', 'alt'=>get_option( 'blogname', false )) ));
									if(!is_front_page()){ ?>
										<?php if(function_exists('icl_get_languages')){
											$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
										} else {
											$home_url='/';
										} ?>
										<a class="footer__logo" href="<?=$home_url?>"><?= $logo ?></a>
									<?php } else { ?>
										<?= '<div class="footer__logo">'.$logo.'</div>' ?>
									<?php } ?>
									<?php if (!empty($options['socl']['footer_d'])) {
										echo '<div class="footer__info">'.$options['socl']['footer_d'].'</div>';
									} else { ?>
									<div class="footer__info">
										Giammetti Vienna
										<br> Liechtensteinstrasse 11
										<br> 1090 Vienna, Austria
										<br> info@giammetti.com
									</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-md-3 col-sm-4  menu_footer">
								<section class="footer-section footer-section_list">
									<h3 class="footer-section__title"><?=__( 'Menu', 'giammetti' )?></h3>
									<?php generic_footer_nav(); ?>
								</section>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<section class="footer-section footer-section_form">
									<h3 class="footer-section__title"><?=__( 'newsletter', 'giammetti' )?></h3>
									<div class="footer-form__info"><?=__( 'Watch our news. Enter your e-mail address and you will always receive the latest news', 'giammetti' )?></div>
									<?php
									$subscr_form=(!empty($options['socl']['subscr_sc']))?$options['socl']['subscr_sc']:'[contact-form-7 id="21" title="Subscribe form" html_class="footer-form"]';
									echo do_shortcode($subscr_form);
									?>
								</section>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="copyright"><?=__( 'Copyright', 'giammetti' )?> © 2017<?=(date('Y')!=='2017')?'-'.date('Y'):null?>. <?=__( 'All Rights Reserved', 'giammetti' )?> </div>
						</div>
					</div>
				</div>
			</footer>
			<div></div>
		</div>
		<div class="slidebar" off-canvas="mobile-slidebar left overlay">
			<?php main_nav(); ?>
		</div>
		<?php
		wp_footer();
		if(!empty($options['advd']['custom_css'])){
			echo ($options['advd']['custom_css']!==' ') ? '<style>'.htmlspecialchars_decode($options['advd']['custom_css']).'</style>' : null;
		}
		if(!empty($options['advd']['custom_js'])){
			echo ($options['advd']['custom_js']!==' ') ? '<script>'.htmlspecialchars_decode($options['advd']['custom_js']).'</script>' : null;
		}
		if(!empty($options['advd']['custom_js_tag'])){
			echo ($options['advd']['custom_js_tag']!==' ') ? str_replace('\"', '"', $options['advd']['custom_js_tag']) : null;
		}
		if(!empty($options['advd']['custom_noscript'])){
			echo ($options['advd']['custom_noscript']!==' ') ? '<noscript>'.str_replace('\\\'', '\'', $options['advd']['custom_noscript']).'</noscript>' : null;
		}
		?>
	</body>
</html>
